/*
 Navicat Premium Data Transfer

 Source Server         : store
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : electronic_components

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 11/01/2024 14:36:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for electronic_components
-- ----------------------------
DROP TABLE IF EXISTS `electronic_components`;
CREATE TABLE `electronic_components`  (
  `componentId` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `imageUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `specification` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `supplierId` int(11) NULL DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `serialNumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `quantity` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`componentId`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of electronic_components
-- ----------------------------
INSERT INTO `electronic_components` VALUES (1, 'Capacitor', '1μF', 'http://127.0.0.1:8888/static/upload/4_Capacitor_1μF_微信图片_20240108192104.jpg', '1μF, 25V', 'A general-purpose capacitor', 4, '0.80', 'R00001', 99);
INSERT INTO `electronic_components` VALUES (3, 'Diode', '1N4007', 'http://127.0.0.1:8888/static/upload/3_1_测试手机_loginbg.png', '1A, 700V', 'A general-purpose diode', 5, '1.20', 'R00001', 100);
INSERT INTO `electronic_components` VALUES (4, 'Inductor', '10mH', 'http://127.0.0.1:8888/static/upload/4_Inductor_10mH_微信图片_20240108192104.jpg', '10mH, 5% tolerance', 'A general-purpose inductor', 4, '1.50', 'R00001', 100);
INSERT INTO `electronic_components` VALUES (5, 'LED', 'Red', 'http://127.0.0.1:8888/static/upload/5_LED_Red_微信图片_20240108192104.jpg', '60mA, 5V', 'A red LED', 5, '0.20', 'R00001', 100);
INSERT INTO `electronic_components` VALUES (6, 'Transistor', 'NPN', 'http://127.0.0.1:8888/static/upload/20_1_bg.png', '200mA, 30V', 'A general-purpose NPN transistor', 6, '0.70', 'R00001', 100);
INSERT INTO `electronic_components` VALUES (7, 'IC', 'LM386', 'http://127.0.0.1:8888/static/upload/20_1_bg.png', 'Audio amplifier IC', 'An audio amplifier integrated circuit', 7, '2.50', 'R00001', 100);
INSERT INTO `electronic_components` VALUES (8, 'Relay', 'Single- Pole', 'http://127.0.0.1:8888/static/upload/20_1_bg.png', '10A, 250VAC', 'A single-pole relay', 8, '3.80', 'R00001', 100);
INSERT INTO `electronic_components` VALUES (9, 'Potentiometer', '1kΩ Linear Trimmer', 'http://127.0.0.1:8888/static/upload/3_1_测试手机_loginbg.png', '1kΩ, Linear Trimmer', 'A linear potentiometer trimmer for calibration', 9, '2.00', 'R00001', 100);
INSERT INTO `electronic_components` VALUES (11, 'Tact Switch', 'Momentary Pushbutton', 'http://127.0.0.1:8888/static/upload/20_1_bg.png', '250mA, 24VDC', 'A momentary pushbutton tact switch', 10, '1.20', 'R00002', 100);

SET FOREIGN_KEY_CHECKS = 1;
