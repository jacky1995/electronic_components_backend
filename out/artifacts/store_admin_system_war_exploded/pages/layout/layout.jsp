<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/1/11 0011
  Time: 18:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 新 Bootstrap5 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/css/bootstrap.min.css">

    <!--  popper.min.js 用于弹窗、提示、下拉菜单 -->
    <script src="https://cdn.staticfile.org/popper.js/2.9.3/umd/popper.min.js"></script>

    <!-- 最新的 Bootstrap5 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.min.js"></script>
    <title>Document</title>
    <style>
        html,
        body {
            padding: 0;
            margin: 0;
            height: 100%;
        }
    </style>
</head>

<body>
<div class="d-flex flex-column" style=" height: 100%;border:2px solid #ae00e6">
    <div region="north" class="north" style="height: 80px;background-color:bisque">
        北
    </div>
    <div region="center" class="center flex-fill d-flex flex-column">
        <div class="d-flex flex-row flex-fill" style=" height: 100%;border:1px solid blue">
            <div region="west" class="west" style="width: 140px;background-color:antiquewhite">
                西
            </div>
            <div region="center" class="center flex-fill" style="background-color: #ae00e6;">
                中
            </div>
            <!-- <div region="east" class="east" style="width: 140px;">
                东
            </div> -->
        </div>
    </div>
    <div region="south" class="south" style="height: 80px;background-color:blueviolet">
        南
    </div>
</div>

</body>

</html>
