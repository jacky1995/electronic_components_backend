<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>用户</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.bundle.min.js"></script>


</head>
<body>
<div class="container" style="width: 400px;">
    ${userList}

    <div class="table-responsive">
        <table class="table" border="1">

            <thead>
            <tr>
                <th>用户名</th>
                <th>性别</th>
                <th>电话</th>

            </tr>
            </thead>
            <tbody>

            <c:forEach items="${userList}" var="user">
                <tr>

                    <td>${user.user_name}</td>
                    <td>${user.user_sex}</td>
                    <td>${user.user_phone}</td>

                </tr>
            </c:forEach>

            </tbody>
        </table>

    </div>

</div>
</body>
</html>