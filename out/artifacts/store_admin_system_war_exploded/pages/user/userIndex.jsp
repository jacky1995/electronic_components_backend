<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>用户管理</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
    <meta http-equiv="description" content="This is my page" />
    <link href="${ctx}/css/css.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css"
          href="${ctx}/js/ligerUI/skins/Aqua/css/ligerui-dialog.css" />
    <link href="${ctx}/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet"
          type="text/css" />
    <script type="text/javascript" src="${ctx }/js/jquery-1.11.0.js"></script>
    <script type="text/javascript" src="${ctx }/js/jquery-migrate-1.2.1.js"></script>
    <script src="${ctx}/js/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="${ctx}/js/ligerUI/js/plugins/ligerDrag.js"
            type="text/javascript"></script>
    <script src="${ctx}/js/ligerUI/js/plugins/ligerDialog.js"
            type="text/javascript"></script>
    <script src="${ctx}/js/ligerUI/js/plugins/ligerResizable.js"
            type="text/javascript"></script>
    <link href="${ctx}/css/pager.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="${ctx}/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctx}/css/public.css" media="all">
    <script src="${ctx}/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
    <link href="${ctx}/css/bootstrap.min.css" rel="stylesheet">
    <script src="${ctx}/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">

        function toPage(pageIndex)
        {
            $("#pageIndex").attr("value",pageIndex);
            $("#userform").attr("action", "${ctx}/userIndex");
            $("#userform").submit();
        }

        function pagerJump()
        {
            var page_size=$('#pager_jump_page_size').val();

            var regu = /^[1-9]\d*$/;``

            if (!regu.test(page_size)||page_size < 1 || page_size >${resultMap.page.total})
            {
                alert('请输入[1-'+ ${resultMap.page.total} +']之间的页码！');
            }else
            {
                $("#pageIndex").attr("value",page_size);
                $("#userform").attr("action", "${ctx}/userIndex");
                $("#userform").submit();
            }
        }

        $(function(){
            /** 获取上一次选中的部门数据 */
            var boxs  = $("input[type='checkbox'][id^='box_']");

            /** 给全选按钮绑定点击事件  */
            $("#checkAll").click(function(){
                // this是checkAll  this.checked是true
                // 所有数据行的选中状态与全选的状态一致
                boxs.attr("checked",this.checked);
            })

            /** 给数据行绑定鼠标覆盖以及鼠标移开事件  */
            $("tr[id^='data_']").hover(function(){
                $(this).css("backgroundColor","#eeccff");
            },function(){
                $(this).css("backgroundColor","#ffffff");
            })


            /** 删除员工绑定点击事件 */
            $("#delete").click(function(){
                /** 获取到用户选中的复选框  */
                var checkedBoxs = boxs.filter(":checked");
                if(checkedBoxs.length < 1){
                    alert("请选择一个需要删除的用户！");
                }else{
                    /*  			   var id_list = [];
                                    for(var i=0;i<checkedBoxs.length;i++){
                                       var id = checkedBoxs[i].value;
                                      id_list.push(id)
                                    }
                                      $("#id").attr("id", id_list); */
                    $("#pageIndex")[0].value=1;
                    $("#userform").attr("action", "${ctx}/userdel.action");
                    $("#userform").submit();
                }
            })

            $("#query").click(function(){
                $("#pageIndex")[0].value=1;
                $("#userform").attr("action", "${ctx}/queryUser.action");
                $("#userform").submit();
            })


        })
        var user_sex = "${sessionScope.user_sex}";
        layui.use(['form', 'table'], function () {
            var $ = layui.jquery,
                form = layui.form,
                table = layui.table;
                var list=[{id:"男",name:"男"},{id:"女",name:"女"}];
                var select=document.getElementById("typeId");

                    for(var obj in list){
                        var option=document.createElement("option");
                        option.setAttribute("value",list[obj].id);
                        option.innerText=list[obj].name;
                        if(user_sex ==list[obj].name){
                            option.selected = "selected";
                        }


                        select.appendChild(option);

                    }

                form.render('select');





        });
    </script>
</head>
<body>
<div class="layuimini-container">
    <form name="userform" method="post" id="userform"
          action="${ctx}/userIndex">
        <input type="hidden" name="pageNum" value="${resultMap.page.pageNum}" id="pageIndex">
    <div class="layuimini-main">
        <div class="demoTable">
            <div class="layui-form-item layui-form ">
                用户名：
                <div class="layui-inline">
                    <input class="layui-input" name="user_name" id="isbn" autocomplete="off" value="${sessionScope.user_name}">
                </div>
                性别：

                <div class="layui-inline">
                    <select id="typeId" name="user_sex" lay-verify="required">
                        <option value="">请选择</option>
                    </select>
                </div>

                <button class="layui-btn" data-type="reload" type="submit">搜索</button>
            </div>
        </div>
    <div>
    <table  border="1" class="table table-hover table-bordered" style="text-align: center">
        <thead>
        <tr>
            <th><input type="checkbox" name="checkAll" id="checkAll"></th>
            <th>用户名</th>
            <th>性别</th>
            <th>城市</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${resultMap.data}" var="user"
                   varStatus="stat">
            <tr id="data_${stat.index}" align="center"
            >
                <td><input type="checkbox" id="box_${stat.index}"
                           value="${user.user_id}" name="userIds" /></td>
                <td>${user.user_name}</td>
                <td>${user.user_sex}</td>
                <td>${user.user_dwell}</td>


                                                <td align="center" width="40px;"><a
                                                        href="${ctx}/viewUser.action?id=${user.user_id}"> <img
                                                        title="修改" src="${ctx}/images/update.gif" /></a></td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</div>
    <div>
        <%@include file="../page/page.jsp"%>
</div>
    </div>
        </form>
</div>

</body>
</html>