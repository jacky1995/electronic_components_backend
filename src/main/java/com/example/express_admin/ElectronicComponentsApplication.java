package com.example.express_admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ServletComponentScan
//@ServletComponentScan(basePackages = "com.filter.Filter")
@EnableConfigurationProperties
@EnableJpaRepositories
@MapperScan("com.example.express_admin.repository")
public class ElectronicComponentsApplication {

	public static void main(String[] args) throws Exception {


		SpringApplication.run(ElectronicComponentsApplication.class, args);
	}

}
