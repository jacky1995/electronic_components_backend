package com.example.express_admin.config;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.Config;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.util.Properties;

/**
 * @PACKAGE_NAME: com.example.express_admin.config
 * @NAME: AlipayFactoryConfig
 * @USER: jacky
 * @DATE: 2023/4/27
 * @TIME: 14:53
 * @YEAR: 2023
 * @MONTH: 04
 * @MONTH_NAME_SHORT: 4月
 * @MONTH_NAME_FULL: 四月
 * @DAY: 27
 * @DAY_NAME_SHORT: 周四
 * @DAY_NAME_FULL: 星期四
 * @HOUR: 14
 * @MINUTE: 53
 * @PROJECT_NAME: express-admin-system
 */
@Configuration
public class AlipayFactoryConfig {

    public AlipayFactoryConfig(AlipayConfig ali) {
        // 设置参数（全局只需设置一次）
        Config config = new Config();
        config.protocol = ali.getProtocol();
        config.gatewayHost = ali.getGatewayHost();
        config.signType = ali.getSignType();
        config.appId = ali.getAppId();
        config.merchantPrivateKey = ali.getMerchantPrivateKey();
        config.alipayPublicKey = ali.getAlipayPublicKey();
        config.notifyUrl = ali.getNotifyUrl();
        Factory.setOptions(config);
        System.out.println("=======支付宝SDK初始化成功=======");
    }

}
