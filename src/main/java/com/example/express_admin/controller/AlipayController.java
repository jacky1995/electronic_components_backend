package com.example.express_admin.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;

import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.alipay.easysdk.factory.Factory;
import com.example.express_admin.config.AlipayConfig;
import com.example.express_admin.pojo.AliPay;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.mapper.PackageMapper;
import com.example.express_admin.service.PackageInfoService;
import com.example.express_admin.utils.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Controller
public class AlipayController {
    @Autowired
    private PackageInfoService packageService;
    private  String GATEWAY_URL= "https://openapi.alipaydev.com/gateway.do";
    @Autowired
    AlipayConfig alipayConfig;
    @Autowired
    private PackageMapper packageMapper;
    @RequestMapping("/toAliPay")
    @ResponseBody
    public String alipay(AliPay aliPay, HttpServletResponse httpResponse) throws IOException {
        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL, alipayConfig.getAppId(),
                alipayConfig.getAppPrivateKey(), "json", alipayConfig.getCharSet(), alipayConfig.getAlipayPublicKey(), alipayConfig.getSignType());
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        request.setNotifyUrl(alipayConfig.getNotifyUrl());
        request.setBizContent("{\"out_trade_no\":\"" + aliPay.getTraceNo() + "\","
                + "\"total_amount\":\"" + aliPay.getTotalAmount() + "\","
                + "\"subject\":\"" + aliPay.getSubject() + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String form = "";
        try {
            form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return form;
    }
//    退款接口
    @RequestMapping("/toRefund")
    @ResponseBody
    public Result refund(AliPay aliPay, @RequestBody Map<String,String> requestData) throws IOException, AlipayApiException {



        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL, alipayConfig.getAppId(),
                alipayConfig.getAppPrivateKey(), "json", alipayConfig.getCharSet(), alipayConfig.getAlipayPublicKey(), alipayConfig.getSignType());
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        //商户订单号，必填

        String tradeNo = requestData.get("tradeNo");
        String userId = requestData.get("userId");
        String refund_amount = requestData.get("expressCosts");

        //标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传
        String out_request_no = new String(UUID.randomUUID().toString());
        JSONObject bizContent = new JSONObject();
        System.out.println(tradeNo);
        System.out.println(refund_amount);
        bizContent.put("trade_no", tradeNo);  // 支付宝回调的订单流水号
        bizContent.put("refund_amount", refund_amount);  // 订单的总金额
        bizContent.put("out_request_no", out_request_no);   //  我的订单编号
        request.setBizContent(bizContent.toString());
        AlipayTradeRefundResponse response = alipayClient.execute(request);
        JSONArray dataArray = new JSONArray();

        if(response.isSuccess()){
            System.out.println("调用成功");
            // 4. 更新数据库状态
            Integer packageId = Integer.valueOf(requestData.get("packageId"));
            packageMapper.updatePackageStatusIdBypackageId(7, packageId);

            dataArray.add(packageService.queryPackageList(1,"",null,null, Integer.valueOf(userId)));

            return Result.success(dataArray);
        } else {
            System.out.println("调用失败");
            return Result.failure(null);
        }



    }
    @RequestMapping("/notify")
    @ResponseBody
    public String payNotify(HttpServletRequest request) throws Exception {
        if (request.getParameter("trade_status").equals("TRADE_SUCCESS")) {
            System.out.println("=========支付宝异步回调========");
            Map<String, String> params = new HashMap<>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (String name : requestParams.keySet()) {
                System.out.println(name);
                params.put(name, request.getParameter(name));
                // System.out.println(name + " = " + request.getParameter(name));
            }

            String tradeNo = params.get("out_trade_no");
            String gmtPayment = params.get("gmt_payment");
            String alipayTradeNo = params.get("trade_no");
//            在这里更新数据库订单状态
//            // 支付宝验签
            if (Factory.Payment.Common().verifyNotify(params)) {
                // 验签通过
                System.out.println("包裹号: " + params.get("packageId"));
                System.out.println("交易名称: " + params.get("subject"));
                System.out.println("交易状态: " + params.get("trade_status"));
                System.out.println("支付宝交易凭证号: " + params.get("trade_no"));
                System.out.println("商户订单号: " + params.get("out_trade_no"));
                System.out.println("交易金额: " + params.get("total_amount"));
                System.out.println("买家在支付宝唯一id: " + params.get("buyer_id"));
                System.out.println("买家付款时间: " + params.get("gmt_payment"));
                System.out.println("买家付款金额: " + params.get("buyer_pay_amount"));
                String[] out_trade_no_list = params.get("out_trade_no").split("_");
                Integer packageId = Integer.valueOf(out_trade_no_list[out_trade_no_list.length-1]);
                String userId = out_trade_no_list[out_trade_no_list.length-2];
                System.out.println("推送");
                System.out.println(userId);
                packageMapper.updatePackageOrderStatusIdBypackageId(6, packageId,alipayTradeNo);
                JSONArray dataArray = new JSONArray();
                WebSocketServer.sendInfo(JSONArray.toJSONString(dataArray), String.valueOf(userId));
            }
        }
        return "success";
    }

    public static void main(String[] args) {
        System.out.println(1);
    }
}
