package com.example.express_admin.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.utils.RSAUtil;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.express_admin.pojo.Result2;
@Controller    // 标注为Spring组件
public class CheckCodeController {

    @RequestMapping("/checkcode")  // 标注访问地址
    @ResponseBody    // 标注返回结果位JSON串
    public String getCode(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int imgwidth=100;
        int imgheight=40;
        //1.创建图片对象，在内存中图片（验证码图片对象）
        BufferedImage image=new BufferedImage(imgwidth,imgheight,BufferedImage.TYPE_INT_RGB);  //也可以指定读取image=imageIO.read(new file())
        //2.美化图片
        Graphics g=image.getGraphics(); //获得画笔对象

        //设置画笔颜色
        g.setColor(Color.pink);
        //在创建的图片对象大小中填充矩形，颜色为上面设置的颜色，第一,二个参数是起始点的x,y,第三，四个参数是有多宽，有多高
        g.fillRect(0, 0, imgwidth, imgheight);

        //重新设置画笔颜色
        g.setColor(Color.yellow);//画框边缘颜色
        //在image上画边框，第一,二个参数是起始点的x,y,第三，四个参数是有多宽，有多高，注意：边框占一个像素，所以需要宽和高-1才能覆盖全部
        g.drawRect(0, 0, imgwidth-1, imgheight-1);

        //随机设置验证码的值
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        Random random=new Random();
        StringBuilder sb=new StringBuilder();
        //随机在image中写字符串，第三，四个参数是画的位置
        Color[] color={Color.BLACK,Color.BLUE,Color.CYAN,Color.GREEN,      //定义颜色数组
                Color.ORANGE,Color.YELLOW,Color.RED,Color.PINK,Color.LIGHT_GRAY};
        for(int i=1;i<5;i++) {
            int index=random.nextInt(str.length());  //随机选取字母字符
            g.setFont(new Font("宋体", Font.PLAIN, 20));  //设置画笔大小
            sb.append(str.charAt(index));//将随机验证码置于stringbuilder中
//            g.setColor(color[random.nextInt(color.length)]);  //画笔颜色
            g.setColor(color[0]);  //画笔颜色
            g.drawString(str.charAt(index)+"",imgwidth/5*i ,25);
        }

        //将验证码存储与session对象中,用于loginservlet中的验证码验证
        String session_code=sb.toString();
        req.getSession().setAttribute("session_code", session_code);

        //随机画干扰线,第一,二个参数是起始点的x,y,第三，四个参数是最后一个点的x,y
        int x1=0,y1=0,x2=0,y2=0;
        for(int i=0;i<=8;i++) {  //画8次线条
            x1=random.nextInt(imgwidth);
            y1=random.nextInt(imgheight);
            x2=random.nextInt(imgwidth);
            y2=random.nextInt(imgheight);
            g.setColor(Color.gray);
            g.drawLine(x1, y1, x2, y2);
        }

        //3.图片显示在页面上
        System.out.println("获取验证码");
        ImageIO.write(image, "jpg", resp.getOutputStream());
        return "";
    }
    @RequestMapping("/verifyCode")  // 标注访问地址
    @ResponseBody    // 标注返回结果位JSON串
    public Result<JSONArray> getverifyCode(HttpServletRequest req, HttpServletResponse resp) throws IOException, JSONException {
        int imgwidth=100;
        int imgheight=40;
        //1.创建图片对象，在内存中图片（验证码图片对象）
        BufferedImage image=new BufferedImage(imgwidth,imgheight,BufferedImage.TYPE_INT_RGB);  //也可以指定读取image=imageIO.read(new file())
        //2.美化图片
        Graphics g=image.getGraphics(); //获得画笔对象

        //设置画笔颜色
        g.setColor(Color.pink);
        //在创建的图片对象大小中填充矩形，颜色为上面设置的颜色，第一,二个参数是起始点的x,y,第三，四个参数是有多宽，有多高
        g.fillRect(0, 0, imgwidth, imgheight);

        //重新设置画笔颜色
        g.setColor(Color.yellow);//画框边缘颜色
        //在image上画边框，第一,二个参数是起始点的x,y,第三，四个参数是有多宽，有多高，注意：边框占一个像素，所以需要宽和高-1才能覆盖全部
        g.drawRect(0, 0, imgwidth-1, imgheight-1);

        //随机设置验证码的值
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        Random random=new Random();
        StringBuilder sb=new StringBuilder();
        //随机在image中写字符串，第三，四个参数是画的位置
        Color[] color={Color.BLACK,Color.BLUE,Color.CYAN,Color.GREEN,      //定义颜色数组
                Color.ORANGE,Color.YELLOW,Color.RED,Color.PINK,Color.LIGHT_GRAY};
        for(int i=1;i<5;i++) {
            int index=random.nextInt(str.length());  //随机选取字母字符
            g.setFont(new Font("宋体", Font.PLAIN, 20));  //设置画笔大小
            sb.append(str.charAt(index));//将随机验证码置于stringbuilder中
//            g.setColor(color[random.nextInt(color.length)]);  //画笔颜色
            g.setColor(color[0]);  //画笔颜色
            g.drawString(str.charAt(index)+"",imgwidth/5*i ,25);
        }

        //将验证码存储与session对象中,用于loginservlet中的验证码验证
        String session_code=sb.toString();
        req.getSession().setAttribute("session_code", session_code);

        //随机画干扰线,第一,二个参数是起始点的x,y,第三，四个参数是最后一个点的x,y
        int x1=0,y1=0,x2=0,y2=0;
        for(int i=0;i<=8;i++) {  //画8次线条
            x1=random.nextInt(imgwidth);
            y1=random.nextInt(imgheight);
            x2=random.nextInt(imgwidth);
            y2=random.nextInt(imgheight);
            g.setColor(Color.gray);
            g.drawLine(x1, y1, x2, y2);
        }

        //3.图片显示在页面上
        System.out.println("获取验证码");

//        ImageIO.write(image, "jpg", resp.getOutputStream());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        //        转成base64
        try {
            // 设置图片格式
            ImageIO.write(image, "jpg", stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = Base64.encodeBase64(stream.toByteArray());
        String base64 = new String(bytes);

        JSONObject veryCodeObj = new JSONObject();
        JSONArray dataArray = new JSONArray();
        veryCodeObj.put("bas64_data","data:image/jpeg;base64," + base64);
        veryCodeObj.put("verify_str",session_code);
        dataArray.add(veryCodeObj);
        return Result.success(dataArray);
    }
    @RequestMapping("/getPem")  // 标注访问地址
    @ResponseBody    // 标注返回结果位JSON串
    public HashMap getPem(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        RSAUtil rSAUtil = new RSAUtil();
        HashMap pemHash = new HashMap();
        pemHash.put("privatePem",rSAUtil.privatePem);
        pemHash.put("publicKeyPem",rSAUtil.privatePem);

        return pemHash;
    }
    public static void main(String[] args) {
        Result2 resultData = new Result2();

        resultData.setError(0);
        resultData.setData(new JSONArray());
        resultData.setMsg("成功");
        System.out.println(resultData);
    }
}
