package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;
import com.example.express_admin.mapper.*;

import com.example.express_admin.pojo.ElectronicComponentsApplication;
import com.example.express_admin.pojo.ElectronicComponentsModel;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.service.ElectronicComponentsService;
import com.example.express_admin.utils.FileUtils;
import com.example.express_admin.utils.PageModel;
import com.example.express_admin.utils.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller   // 标注为Spring组件
public class ComponentsController {




    ElectronicComponentsModel electronicComponentsModel = new ElectronicComponentsModel();
    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ElectronicComponentsMapper electronicComponentsMapper;
    @Autowired
    private SupplierMapper supplierMapper;
    private String uplodaDirectory = System.getProperty("user.dir") + "/src/main/webapp/static/upload/";
    private String networdPath = "http://127.0.0.1:8888/static/upload/";
    @Autowired
    private ElectronicComponentsService electronicComponentsService;
    FileUtils fileUtils = new FileUtils();


    @RequestMapping("/componentsManagementIndex")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> componentsManagementIndex(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        Integer pageIndex = 1;
        if (requestData.get("pageNum") != null) {
            pageIndex = Integer.parseInt(requestData.get("pageNum"));
        }
        if (requestData.get("pageSize") != null) {
            Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
            PageModel pageModel = new PageModel();
            pageModel.setPageSize(pageSize);
        }


        String keyValue = requestData.get("keyValue");


        session.setAttribute("keyValue", keyValue);


        JSONArray dataArray = new JSONArray();


        dataArray.add(electronicComponentsService.queryAllComponentsList(pageIndex, keyValue));

        return Result.success(dataArray);

    }
    @RequestMapping("/checkComponent")  // 标注访问地址
    @ResponseBody
    public HashMap checkComponent(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HashMap result2 = new HashMap();

        result2.put("msg", "");
        result2.put("error", 0);
        HashMap userMap = new HashMap();
        if (requestData.get("componentId") != "") {
            String componentId = requestData.get("componentId");
            electronicComponentsModel = electronicComponentsMapper.queryComponentByComponentId(componentId);
            userMap.put("component_data", electronicComponentsModel);
        } else {
            userMap.put("component_data", electronicComponentsModel);
        }

        userMap.put("supplier_data", supplierMapper.querySupplierList());
        result2.put("data", userMap);
        return result2;

    }
    @RequestMapping("/checkApplication")  // 标注访问地址
    @ResponseBody
    public HashMap checkApplication(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HashMap result2 = new HashMap();

        result2.put("msg", "");
        result2.put("error", 0);
        String componentId = requestData.get("componentId");
        Integer applicationStatusId = Integer.valueOf(requestData.get("applicationStatusId"));
        result2.put("data", electronicComponentsService.checkApplication(componentId,applicationStatusId));
        return result2;

    }
    @RequestMapping("/editpplication")  // 标注访问地址
    @ResponseBody
    public HashMap editpplication(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HashMap result2 = new HashMap();
        HashMap application_data = new HashMap();

        result2.put("msg", "");
        result2.put("error", 0);
        String applicationId = requestData.get("applicationId");
        application_data = electronicComponentsService.queryApplication(applicationId);
        application_data.put("user_data", userMapper.queryAllUserList());
        result2.put("data", application_data);


        return result2;

    }

    @RequestMapping("/OutOrInStore")  // 标注访问地址
    @ResponseBody
    public HashMap OutOrInStore(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HashMap result2 = new HashMap();

        result2.put("msg", "");
        result2.put("error", 0);
        HashMap userMap = new HashMap();

        if (requestData.get("componentId") != "") {
            String componentId = requestData.get("componentId");
            electronicComponentsModel = electronicComponentsMapper.queryComponentByComponentId(componentId);
            userMap.put("component_data", electronicComponentsModel);
        } else {
            userMap.put("component_data", electronicComponentsModel);
        }

        userMap.put("user_data", userMapper.queryAllUserList());
        result2.put("data", userMap);
        return result2;

    }

//    新增
    @RequestMapping("/componentInsert")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> packageInsert(String name,Integer quantity,
                                           String model,String componentId,
                                           String specification, String serialNumber, String description, Integer supplierId,
                                           String price,
                                           MultipartFile file, HttpServletRequest request) throws IOException {

        HashMap result2 = new HashMap();

        String fileName = file.getOriginalFilename();
        String totalFileName = String.valueOf(supplierId) + "_" + name + "_" + model+ "_" + fileName;
        fileUtils.writeFileToLocal(file, uplodaDirectory + totalFileName);
        String componentImageUrl = networdPath + totalFileName;
//        Integer componentId =electronicComponentsMapper.queryComponentNum()+1;

//        Integer componentId = (int) System.currentTimeMillis();
//        先查重
        electronicComponentsModel = electronicComponentsMapper.queryComponentByComponentId(componentId);
        System.out.println(electronicComponentsModel==null);
        if(electronicComponentsModel==null){
            int result_num = electronicComponentsMapper.insertComponent(componentId,name,
                    model ,componentImageUrl,
                    specification,  serialNumber,  description,  supplierId,
                    price,quantity);
            if (result_num > 0) {

                result2.put("error", 0);
            } else {
                result2.put("error", 1);
            }
            JSONArray dataArray = new JSONArray();
            dataArray.add(electronicComponentsService.queryAllComponentsList(1, ""));
            return Result.success(dataArray);
        }else{
            JSONArray dataArray = new JSONArray();

            return Result.success(dataArray);
        }



    }

//    修改
    @RequestMapping("/componentUpdate")  // 标注访问地址

    @ResponseBody
    public Result<JSONArray> componentUpdate(String componentId,String oldComponentId,String name,String oldPcomponentImageUrl,
                                           String model,
                                           String specification, String serialNumber, String description, Integer supplierId,
                                           String price,Integer quantity,
                                           MultipartFile file, HttpServletRequest request) throws IOException {

        HashMap result2 = new HashMap();
        String componentImageUrl = oldPcomponentImageUrl;
        if (file != null) {
            String fileName = file.getOriginalFilename();

            String totalFileName = String.valueOf(supplierId) + "_" + name + "_" + model+ "_" + fileName;
//          先写入文件
            fileUtils.writeFileToLocal(file, uplodaDirectory + totalFileName);
            componentImageUrl = networdPath + totalFileName;

//            再删除文件

            int lastIndex = oldPcomponentImageUrl.lastIndexOf("/")+1;

            String old_local_image_path= oldPcomponentImageUrl.substring(lastIndex,oldPcomponentImageUrl.length());
            fileUtils.deleteFile(uplodaDirectory + old_local_image_path);
        }
        List<ElectronicComponentsApplication> componentList = electronicComponentsMapper.queryapplicationByapplicationId(componentId);
        int result_num = electronicComponentsMapper.updateComponentByComponentId(oldComponentId,componentId,  name,
                 model,
                 specification,  serialNumber,  description,  supplierId,
                 price,
                 componentImageUrl, quantity-componentList.size());
        if (result_num > 0) {

            result2.put("error", 0);
        } else {
            result2.put("error", 1);
        }

        JSONArray dataArray = new JSONArray();
        dataArray.add(electronicComponentsService.queryAllComponentsList(1, ""));
        return Result.success(dataArray);
    }


//    删除
    @RequestMapping("/componentDel")  // 标注访问地址

    @ResponseBody
    public Result<JSONArray> componentDel(@RequestBody ElectronicComponentsModel electronicComponentsModel) throws IOException {

        HashMap result2 = new HashMap();
        int result_num = electronicComponentsMapper.delComponentNum( electronicComponentsModel.getComponentId());
        if (result_num > 0) {

            result2.put("error", 0);
        } else {
            result2.put("error", 1);
        }
        JSONArray dataArray = new JSONArray();
        dataArray.add(electronicComponentsService.queryAllComponentsList(1, ""));
        return Result.success(dataArray);
    }


    @RequestMapping("/componentsOutOrInManagementIndex")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> componentsOutOrInManagementIndex(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        Integer pageIndex = 1;
        if (requestData.get("pageNum") != null) {
            pageIndex = Integer.parseInt(requestData.get("pageNum"));
        }
        if (requestData.get("pageSize") != null) {
            Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
            PageModel pageModel = new PageModel();
            pageModel.setPageSize(pageSize);
        }


        String keyValue = requestData.get("keyValue");


        session.setAttribute("keyValue", keyValue);


        JSONArray dataArray = new JSONArray();


        dataArray.add(electronicComponentsService.queryInventoryList(pageIndex, keyValue));

        return Result.success(dataArray);

    }
    @RequestMapping("/queryInventoryList")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> queryInventoryList(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        Integer pageIndex = 1;
        if (requestData.get("pageNum") != null) {
            pageIndex = Integer.parseInt(requestData.get("pageNum"));
        }
        if (requestData.get("pageSize") != null) {
            Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
            PageModel pageModel = new PageModel();
            pageModel.setPageSize(pageSize);
        }


        String keyValue = requestData.get("keyValue");


        session.setAttribute("keyValue", keyValue);


        JSONArray dataArray = new JSONArray();


        dataArray.add(electronicComponentsService.queryInventoryList(pageIndex, keyValue));

        return Result.success(dataArray);

    }

    @RequestMapping("/supplierInsert")  // 标注访问地址
    @ResponseBody
    public HashMap supplierInsert(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        String supplierName = requestData.get("supplierName");

        HashMap result2 = new HashMap();
        HashMap userMap = new HashMap();
        int result_num = electronicComponentsMapper.insertSupplier( supplierName);
        if (result_num > 0) {

            result2.put("error", 0);
        } else {
            result2.put("error", 1);
        }

        userMap.put("supplier_data", supplierMapper.querySupplierList());
        result2.put("data", userMap);
        return result2;

    }

//    产品轮播图
    @RequestMapping("/componentsLoop")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> componentsLoop() throws IOException {


        JSONArray dataArray = new JSONArray();


        dataArray.add(electronicComponentsMapper.queryComponentsLoop());

        return Result.success(dataArray);

    }

}
