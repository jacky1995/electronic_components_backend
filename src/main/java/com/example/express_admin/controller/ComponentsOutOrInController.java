package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;
import com.example.express_admin.mapper.ElectronicComponentsMapper;
import com.example.express_admin.mapper.SupplierMapper;
import com.example.express_admin.mapper.UserMapper;
import com.example.express_admin.pojo.ElectronicComponentsModel;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.service.ElectronicComponentsService;
import com.example.express_admin.utils.FileUtils;
import com.example.express_admin.utils.PageModel;
import com.example.express_admin.utils.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@Controller   // 标注为Spring组件
public class ComponentsOutOrInController {




    ElectronicComponentsModel electronicComponentsModel = new ElectronicComponentsModel();
    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ElectronicComponentsMapper electronicComponentsMapper;
    @Autowired
    private SupplierMapper supplierMapper;
    private String uplodaDirectory = System.getProperty("user.dir") + "/src/main/webapp/static/upload/";
    private String networdPath = "http://127.0.0.1:8888/static/upload/";
    @Autowired
    private ElectronicComponentsService electronicComponentsService;
    FileUtils fileUtils = new FileUtils();


    @RequestMapping("/componentsOutOrIn")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> ComponentsOutOrIn(@RequestBody Map<String, ArrayList> requestData, HttpServletRequest request) throws IOException {
        ArrayList outOrInList = requestData.get("outOrInList");



        JSONArray dataArray = new JSONArray();


        dataArray.add(electronicComponentsService.componentsOutOrInList(outOrInList));

        return Result.success(dataArray);

    }

}
