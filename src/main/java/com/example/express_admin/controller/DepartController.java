package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;
import com.example.express_admin.mapper.*;
import com.example.express_admin.pojo.Depart;
import com.example.express_admin.pojo.Result2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@Controller   // 标注为Spring组件
public class DepartController {

    @Autowired
    private DepartMapper departMapper;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private Depart depart;
//    查询
    @RequestMapping("/departAll")  // 标注访问地址
    @ResponseBody
    public Result2 departAll(@RequestBody Map<String,String> requestData, HttpServletRequest request) {

        Result2 result = new Result2();
        JSONArray dataArray = new JSONArray();
//        System.out.println( mongoTemplate.findAll(Depart.class));

        dataArray.add(departMapper.queryAllDepart());
        result.setData(dataArray);
        result.setMsg("");
        result.setError(0);
        return result;

    }


}
