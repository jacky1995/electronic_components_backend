package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;

import com.example.express_admin.mapper.*;
import com.example.express_admin.pojo.PackageInfo;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.pojo.Result2;
import com.example.express_admin.service.PackageInfoService;
import com.example.express_admin.utils.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;


@Controller   // 标注为Spring组件
public class ExpressController {


    @Autowired
    private PackageMapper packageMapper;
    @Autowired
    private PackageStatusMapper packageStatusMapper;
    @Autowired
    private PackageTypeMapper packageTypeMapper;
    @Autowired
    private PackageCarMapper packageCarMapper;
    @Autowired
    private ExpressCompanyMapper expressCompanyMapper;


    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private UserMapper userMapper;
    private String uplodaDirectory = System.getProperty("user.dir") + "/src/main/webapp/static/upload/";
    private String networdPath = "http://127.0.0.1:8888/static/upload/";
    @Autowired
    private PackageInfoService packageService;
    FileUtils fileUtils = new FileUtils();
//    @RequestMapping("/packageIndex")  // 标注访问地址
//    @ResponseBody
//    public Result sampleIndex(@RequestBody Map<String,String> requestData,HttpServletRequest request) {
//        HttpSession session=request.getSession();
//        Integer pageIndex = 1;
//        if(requestData.get("pageNum")!=null) {
//            pageIndex = Integer.parseInt(requestData.get("pageNum"));
//        }
//        if(requestData.get("pageSize")!=null) {
//            Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
//            PageModel pageModel =new PageModel();
//            pageModel.setPageSize(pageSize);
//        }
//
//
//        String keyValue = requestData.get("keyValue");
//
//        session.setAttribute("keyValue", keyValue);
//
//        Result result = new Result();
//        JSONArray dataArray = new JSONArray();
//        dataArray.add(packageRepositoryMapper.queryAllSampleTypeList());
//        result.setData(dataArray);
//        result.setMsg("");
//        result.setError(0);
//        return result;
//
//    }
//    通过单号查询
    @RequestMapping("/queryPackageBypackageNumber")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> queryPackageBypackageNumber(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        String keyValue = requestData.get("keyValue");


        session.setAttribute("keyValue", keyValue);
        Integer userId = null;
        Integer packageStatusId = null;
        Integer packageApproveStatusId = null;

        JSONArray dataArray = new JSONArray();


        dataArray.add(packageService.queryPackageBypackageNumber(keyValue));

        return Result.success(dataArray);

    }
    @RequestMapping("/packageManagementIndex")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> packageManagementIndex(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        Integer pageIndex = 1;
        if (requestData.get("pageNum") != null) {
            pageIndex = Integer.parseInt(requestData.get("pageNum"));
        }
        if (requestData.get("pageSize") != null) {
            Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
            PageModel pageModel = new PageModel();
            pageModel.setPageSize(pageSize);
        }


        String keyValue = requestData.get("keyValue");


        session.setAttribute("keyValue", keyValue);
        Integer userId = null;
        Integer packageStatusId = null;
        Integer packageApproveStatusId = null;

        JSONArray dataArray = new JSONArray();
        if (requestData.get("packageStatusId") != "") {

            packageStatusId = Integer.valueOf(requestData.get("packageStatusId"));
        }
        if (requestData.get("userId") != "") {

            userId = Integer.valueOf(requestData.get("userId"));
        }

        dataArray.add(packageService.queryPackageList(pageIndex, keyValue, packageStatusId, packageApproveStatusId, userId));

        return Result.success(dataArray);

    }

    @RequestMapping("/inventoryManagementIndex")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> inventoryManagementIndex(@RequestBody Map<String, String> requestData, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer pageIndex = 1;
        if (requestData.get("pageNum") != null) {
            pageIndex = Integer.parseInt(requestData.get("pageNum"));
        }
        if (requestData.get("pageSize") != null) {
            Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
            PageModel pageModel = new PageModel();
            pageModel.setPageSize(pageSize);
        }


        String keyValue = requestData.get("keyValue");


        session.setAttribute("keyValue", keyValue);
        Integer packageStatusId = null;
        Integer packageApproveStatusId = null;
        Integer userId = null;

        JSONArray dataArray = new JSONArray();
        if (requestData.get("packageStatusId") != "") {

            packageStatusId = Integer.valueOf(requestData.get("packageStatusId"));
        }
        if (requestData.get("packageApproveStatusId") != "") {

            packageApproveStatusId = Integer.valueOf(requestData.get("packageApproveStatusId"));
        }


        dataArray.add(packageService.queryPackageList(pageIndex, keyValue, packageStatusId, packageApproveStatusId, userId));
        return Result.success(dataArray);

    }

    //    修改
    @RequestMapping("/packageEdit")  // 标注访问地址
    @ResponseBody
    public HashMap userEdit(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws Exception {

        HashMap result2 = new HashMap();
        PackageInfo packageInfo = new PackageInfo();
        result2.put("msg", "");
        result2.put("error", 0);
        HashMap userMap = new HashMap();
        if (requestData.get("packageId") != "") {
            Integer packageId = Integer.parseInt(requestData.get("packageId"));
            packageInfo = packageMapper.queryPackageByPackageId(packageId);
            userMap.put("package_data", packageInfo);
        } else {
            userMap.put("package_data", packageInfo);
        }
//        加入快递信息

        String expressCompanyCode = (String) packageInfo.getExpressCompanyCode();
        String packageNumber = (String) packageInfo.getPackageNumber();


        userMap.put("express_data", packageService.getPackagePost(packageNumber, expressCompanyCode));
//        userMap.put("express_data",new HashMap<>());
        userMap.put("package_type_data", packageTypeMapper.selectList(null));
        userMap.put("package_car_data", packageCarMapper.selectList(null));
        userMap.put("express_company_data", expressCompanyMapper.selectList(null));
        userMap.put("package_status_data", packageMapper.queryAllPackageStatusList());
        userMap.put("user_data", userMapper.selectList(null));
        result2.put("data", userMap);
        return result2;


    }

    //    //    更新审批状态
    @RequestMapping("/packageApproveStatus")  // 标注访问地址
//    public Result packageUpdate(@RequestBody package package, HttpServletRequest request) {
    @ResponseBody
    public Result<JSONArray> packageUpdateApproveStatus(
            int packageApproveStatusId,
            int packageId,

            MultipartFile file, HttpServletRequest request) throws IOException {

        HashMap result2 = new HashMap();


        int result_num = packageMapper.updateSampleModelApproveStatusBySampleModelId(
                packageId, packageApproveStatusId);
        if (result_num > 0) {

            result2.put("error", 0);
        } else {
            result2.put("error", 1);
        }
        Result2 result = new Result2();
        JSONArray dataArray = new JSONArray();
        dataArray.add(packageService.queryPackageList(1, "", null, 0, null));
        return Result.success(dataArray);
    }

    //    //    更新
    @RequestMapping("/packageUpdate")  // 标注访问地址
//    public Result packageUpdate(@RequestBody package package, HttpServletRequest request) {
    @ResponseBody
    public Result<JSONArray> packageUpdate(int packageId,
                                           String addresseeMobile,
                                           String addresseeAddress,
                                           int expressCompanyId,
                                           int packageStatusId,
                                           String packageName,
                                           String addresseeName,
                                           String packageWeight,
                                           String expressCosts,
                                           String packageNumber,
                                           int packageTypeId,
                                           int packageOperatorId,
                                           String packageImageUrl,
                                           String oldPackageImageUrl,
                                           MultipartFile file) throws IOException {

        HashMap result2 = new HashMap();

        if (file != null) {

            String fileName = file.getOriginalFilename();
            String totalFileName = packageId + "_" + packageName + "_" + fileName;
//          先写入文件
            fileUtils.writeFileToLocal(file, uplodaDirectory + totalFileName);
            packageImageUrl = networdPath + totalFileName;

//            再删除文件



            int lastIndex = oldPackageImageUrl.lastIndexOf("/")+1;

            String old_local_image_path= oldPackageImageUrl.substring(lastIndex,oldPackageImageUrl.length());
            fileUtils.deleteFile(uplodaDirectory + old_local_image_path);
        }
        int result_num = packageMapper.updatepackageBypackageId(
                packageId, expressCompanyId, packageName, packageNumber, packageOperatorId, packageStatusId, addresseeMobile,
                addresseeAddress, addresseeName, packageTypeId, packageImageUrl,packageWeight,expressCosts);
        if (result_num > 0) {

            result2.put("error", 0);
        } else {
            result2.put("error", 1);
        }

        JSONArray dataArray = new JSONArray();

        dataArray.add(packageService.queryPackageList(1, "", null, null, null));

        return Result.success(dataArray);
    }


    //    //    新增
    @RequestMapping("/packageInsert")  // 标注访问地址
//    public Result packageUpdate(@RequestBody package package, HttpServletRequest request) {
    @ResponseBody
    public Result<JSONArray> packageInsert(Integer expressCompanyId, String packageName,
                                           Integer packageStatusId, String addresseeMobile,
                                           String addresseeAddress, String addresseeName, Integer packageTypeId, String packageImageUrl,
                                           String packageWeight, String expressCosts, Integer userId,
                                           MultipartFile file, HttpServletRequest request) throws IOException {

        HashMap result2 = new HashMap();


        String fileName = file.getOriginalFilename();
        String totalFileName = expressCompanyId + "_" + userId + "_" + packageName + "_" + fileName;
        fileUtils.writeFileToLocal(file, uplodaDirectory + totalFileName);
        packageImageUrl = networdPath + totalFileName;
        System.out.println("新增");
        int result_num = packageMapper.insertPackage(expressCompanyId, packageName,
                packageStatusId, addresseeMobile,
                addresseeAddress, addresseeName, packageTypeId, packageImageUrl,
                packageWeight, expressCosts, userId);
        if (result_num > 0) {

            result2.put("error", 0);
        } else {
            result2.put("error", 1);
        }

        JSONArray dataArray = new JSONArray();

        dataArray.add(packageService.queryLastInsertUnpayPackageInfo(userId));
        WebSocketServer.sendInfo(JSONArray.toJSONString(dataArray), String.valueOf(userId));
        return Result.success(dataArray);
    }

    //    //   删除
    @RequestMapping("/packageDel")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> packageDel(@RequestBody PackageInfo packageInfo, HttpServletRequest request) {

        HashMap result2 = new HashMap();

        int result_num = packageMapper.deleteById(packageInfo.getPackageId());
        if (result_num > 0) {

            result2.put("error", 0);

//            删除文件
            String net_image_path = packageInfo.getPackageImageUrl();

            int lastIndex = net_image_path.lastIndexOf("/")+1;

//
            String local_image_path= net_image_path.substring(lastIndex,net_image_path.length());
            fileUtils.deleteFile(uplodaDirectory + local_image_path);
        } else {
            result2.put("error", 1);
        }

        JSONArray dataArray = new JSONArray();

        dataArray.add(packageService.queryPackageList(1, "", null, null, null));
        return Result.success(dataArray);
    }

    @RequestMapping("/findCabinetData")  // 标注访问地址
    @ResponseBody
    public HashMap findCabinetData(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws Exception {

        HashMap result2 = new HashMap();
        ;
        result2.put("msg", "");
        result2.put("error", 0);
        HashMap dataMap = new HashMap();
        packageMapper.findCabinetData();
        dataMap.put("package_cabinet_data", packageMapper.findCabinetData());
        result2.put("data", dataMap);
        return result2;


    }

//    @RequestMapping("/updateCabinetData")  // 标注访问地址
//    @ResponseBody
//    public HashMap updateCabinetData(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws Exception {
//
//        HashMap result2 = new HashMap();
//        ;
//        JSONObject package_cabinet_data = requestData.get("requestData");
//
//        result2.put("msg", "");
//        result2.put("error", 0);
//        HashMap dataMap = new HashMap();
//        packageRepository.findCabinetData();
//        dataMap.put("package_cabinet_data", packageRepository.findCabinetData());
//        result2.put("data", dataMap);
//        return result2;
//
//
//    }

    //    通过用户名和单号查询信息
    @RequestMapping("/findpackageByPackageNumberAndCompanyName")  // 标注访问地址
    @ResponseBody
    public HashMap findpackageByPackageNumberAndCompanyName(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws Exception {

        HashMap result2 = new HashMap();
        ;
        result2.put("msg", "");
        result2.put("error", 0);
        HashMap userMap = new HashMap();
        String expressCompanyCode = (String) requestData.get("expressCompanyCode");
        String packageNumber = (String) requestData.get("packageNumber");
        System.out.println(expressCompanyCode);
        System.out.println(packageNumber);
        userMap.put("express_data", packageService.getPackagePost(packageNumber, expressCompanyCode));
        result2.put("data", userMap);
        return result2;


    }

    //    获取快递公司的数据
    @RequestMapping("/findExpresCompanyList")  // 标注访问地址
    @ResponseBody
    public HashMap findExpresCompanyList(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws Exception {

        HashMap result2 = new HashMap();

        result2.put("msg", "");
        result2.put("error", 0);
        HashMap userMap = new HashMap();
        userMap.put("express_company_data", expressCompanyMapper.selectList(null));
        result2.put("data", userMap);
        return result2;

    }

    //    获取快递状态的数据
    @RequestMapping("/findPackageStatusList")  // 标注访问地址
    @ResponseBody
    public HashMap findPackageStatusList(@RequestBody Map<String, String> requestData, HttpServletRequest request) throws Exception {

        HashMap result2 = new HashMap();
        result2.put("msg", "");
        result2.put("error", 0);
        HashMap userMap = new HashMap();
        userMap.put("package_status_data", packageStatusMapper.selectList(null));
        result2.put("data", userMap);
        return result2;

    }




    @RequestMapping("/packageOutInStore")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> packageOutInStore(@RequestBody Map<String,Object > requestData) throws IOException {
//        如果是待取件，则生成取件码
        Integer packageStatusId = Integer.valueOf(String.valueOf(requestData.get("packageStatusId")));

//        int result_num;
        ArrayList packageList = (ArrayList) requestData.get("packageList");
//        通知取件
        if(packageStatusId==11){
            String packageIdStr = (String) requestData.get("packageIdStr");
            //        packageService.sendFetchEmail(packageList);//邮件通知取件
                      packageMapper.updatePackagepackageStatus(packageStatusId,packageIdStr);
            packageService.sendWeChatNotice(packageList);//邮件通知取件
        }else {
            String cabinetData = (String) requestData.get("package_cabinet_data");
            packageMapper.updatePackageCabinetData(cabinetData);
            packageService.updatePackagepackageStatusAndFetchCode(packageList);
//          packageRepository.updatePackagepackageStatus(packageStatusId,packageIdStr);

        }
        String cabinetData = (String) requestData.get("package_cabinet_data");
        packageMapper.updatePackageCabinetData(cabinetData);

        JSONArray dataArray = new JSONArray();

        dataArray.add(packageService.queryPackageList(1, "", 9, null, null));
        return Result.success(dataArray);
    }

}
