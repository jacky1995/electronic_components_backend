package com.example.express_admin.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller    // 标注为Spring组件
public class HelloWorldController {

    @RequestMapping("/hello")  // 标注访问地址
    @ResponseBody    // 标注返回结果位JSON串
    public String helloSpringBoot() {
        return "SpringBoot,HelloWord!";
    }
}
