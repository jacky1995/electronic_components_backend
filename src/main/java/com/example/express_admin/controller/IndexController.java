package com.example.express_admin.controller;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller  // 标注为Spring组件
public class IndexController {

    @RequestMapping("/index")  // 标注访问地址
    public String index() {

        return "index";
    }

}
