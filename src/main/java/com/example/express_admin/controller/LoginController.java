package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.pojo.Result2;
import com.example.express_admin.pojo.User;
import com.example.express_admin.mapper.UserMapper;
import com.example.express_admin.service.UserService;
import com.example.express_admin.utils.CodeUtils;
import com.example.express_admin.utils.RSAUtil;
import com.example.express_admin.utils.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@Controller    // 标注为Spring组件
public class LoginController {
    @Autowired
    private SendEmailService sendEmailService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;
    @RequestMapping("/login")  // 标注访问地址

    public String login() {

        return "login/login";
    }

    @RequestMapping("/checklogin")  // 标注访问地址
    @ResponseBody    // 标注返回结果位JSON串
    public Result<JSONArray> checklogin(@RequestBody Map<String,String> requestData) throws Exception {




        String user_name = RSAUtil.decrypt( requestData.get("user_name"));
        String user_password = RSAUtil.decrypt( requestData.get("user_password"));
        System.out.println("登录验证---");
        System.out.println(user_name);
        System.out.println(user_password);

        JSONArray dataArray = new JSONArray();
        dataArray.add(userMapper.queryUserByUserNameAndPassWord(user_name,user_password));
        return Result.success(dataArray);
    }

    @RequestMapping("/sendEmailCode")  // 标注访问地址
    @ResponseBody    // 标注返回结果位JSON串
    public Result<JSONArray> sendEmailCode(@RequestBody Map<String,String> requestData) throws IOException {
        String user_email = requestData.get("receiver_email");
        CodeUtils codeUtils = new CodeUtils();
        String code = codeUtils.getStringRandom();
        String to = user_email;
        String subject = "您有一封来自汕头市云上禅端技术开发有限公司的的邮件";
        String text =  String.format(

                "<h1>【汕头市云上禅端技术开发有限公司】验证码:<span style=font-weight: bold;color: red;>%s</span>，此验证码用于汕头市云上禅端技术开发有限公司验证，请勿泄露。<h1> ",code);

        sendEmailService.sendSimpleEmail(to,subject,text);
        JSONArray dataArray = new JSONArray();
        dataArray.add(code);
        return Result.success(dataArray);
    }

    @RequestMapping("/userUpdateInfo")  // 标注访问地址
    @ResponseBody
    public Result2 userUpdateInfo(@RequestBody User requestData, HttpServletRequest request) throws Exception {

        Result2 result = new Result2();
        String userMobile = RSAUtil.decrypt( requestData.getUserMobile());
        String userEmail = RSAUtil.decrypt( requestData.getUserEmail());

        Integer userId =requestData.getUserId();

        try {
            User newUser = new User();
            newUser = userMapper.selectById(userId);
            System.out.println(userId);
            System.out.println(newUser);
            String userPwd = RSAUtil.decrypt( requestData.getUserPwd());
            newUser.setUserPwd(userPwd);
            newUser.setUserMobile(userMobile);
            newUser.setUserEmail(userEmail);
            userMapper.updateUserPwdByUserId(newUser) ;
            result.setMsg("更新成功");
            result.setError(0);
        }catch (Exception e){
            result.setMsg("更新失败");
            result.setError(1);
            e.printStackTrace();

        }
        return result;
    }
}
