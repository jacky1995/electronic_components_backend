package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.mapper.ExpressCompanyMapper;
import com.example.express_admin.mapper.PackageMapper;
import com.example.express_admin.mapper.UserMapper;
import com.example.express_admin.service.PackageInfoService;
import com.example.express_admin.utils.FileUtils;
import com.example.express_admin.utils.PageModel;
import com.example.express_admin.utils.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;


@Controller   // 标注为Spring组件
public class NoticeController {


    @Autowired
    private PackageMapper packageMapper;
    @Autowired
    private ExpressCompanyMapper expressCompanyMapper;

    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private UserMapper userMapper;
    private String uplodaDirectory = System.getProperty("user.dir")+"/src/main/webapp/static/upload/";
    private String networdPath = "http://127.0.0.1:8888/static/upload/";
    @Autowired
    private PackageInfoService packageService;
    FileUtils fileUtils = new FileUtils();


    @RequestMapping("/noticeManagementIndex")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> packageManagementIndex(@RequestBody Map<String,String> requestData, HttpServletRequest request) throws IOException {
        HttpSession session=request.getSession();
        Integer pageIndex = 1;
        if(requestData.get("pageNum")!=null) {
            pageIndex = Integer.parseInt(requestData.get("pageNum"));
        }
        if(requestData.get("pageSize")!=null) {
            Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
            PageModel pageModel =new PageModel();
            pageModel.setPageSize(pageSize);
        }


        String keyValue = requestData.get("keyValue");


        session.setAttribute("keyValue", keyValue);
        Integer userId = null;
        Integer packageStatusId = null;
        Integer packageApproveStatusId = null;

        JSONArray dataArray = new JSONArray();
        if(requestData.get("packageStatusId")!=""){

            packageStatusId = Integer.valueOf(requestData.get("packageStatusId"));
        }
        if(requestData.get("userId")!=""){

            userId = Integer.valueOf(requestData.get("userId"));
        }

        dataArray.add(packageService.queryPackageList(pageIndex,keyValue,packageStatusId,packageApproveStatusId,userId));

        return Result.success(dataArray);

    }






}
