package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;
import com.example.express_admin.pojo.Result;
import com.example.express_admin.pojo.Result2;
import com.example.express_admin.pojo.User;
import com.example.express_admin.mapper.UserMapper;
import com.example.express_admin.service.UserService;
import com.example.express_admin.utils.RSAUtil;
import com.example.express_admin.utils.SendEmailService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller    // 标注为Spring组件
public class RegisterController {
    @Autowired
    private SendEmailService sendEmailService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

//    检查注册信息
    @RequestMapping("/checkRegister")  // 标注访问地址
    @ResponseBody    // 标注返回结果位JSON串
    public Result<JSONArray> checkRegister(@RequestBody Map<String,String> requestData) throws Exception {



        String userName = RSAUtil.decrypt( requestData.get("userName"));
        String userPwd = RSAUtil.decrypt( requestData.get("userPwd"));
        String userMobile = RSAUtil.decrypt( requestData.get("userMobile"));
        String userEmail = RSAUtil.decrypt( requestData.get("userEmail"));
        String userSex = RSAUtil.decrypt( requestData.get("userSex"));
        JSONArray dataArray = new JSONArray();

        Result2 result = new Result2();
        User user = userMapper.queryUserByUserName(userName);
        System.out.println(user!=null);
        if(user!=null){
            result.setMsg("该用户已存在");
            result.setError(1);
//            证明存在
        }else{
//            User newUser = new User();
//            newUser.setUserName(userName);
//            newUser.setUserPwd(userPwd);
//            newUser.setUserEmail(userEmail);
//            newUser.setUserMobile(userMobile);
//            newUser.setUserSex(userSex);
//            System.out.println(newUser);
            int result_num = userMapper.addUser( userName, userPwd,
                   userMobile,
                    userEmail,
                   userSex);
            System.out.println(result_num);
            result.setMsg("注册成功");
            result.setError(0);
//            注册用户
        };
        dataArray.add(result);
        return Result.success(dataArray);
    }




}
