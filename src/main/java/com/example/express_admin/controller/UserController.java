package com.example.express_admin.controller;


import com.alibaba.fastjson.JSONArray;

import com.example.express_admin.pojo.Result;
import com.example.express_admin.pojo.Result2;
import com.example.express_admin.pojo.User;
import com.example.express_admin.mapper.UserMapper;
import com.example.express_admin.mapper.*;
import com.example.express_admin.service.UserService;

import com.example.express_admin.utils.PageModel;
import com.example.express_admin.utils.RSAUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;


@Controller   // 标注为Spring组件
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DepartMapper departMapper;

//    查询
    @RequestMapping("/userIndex")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> userIndex(@RequestBody Map<String,String> requestData, HttpServletRequest request) {
        HttpSession session=request.getSession();
        Integer pageIndex = 1;
        if(requestData.get("pageNum")!=null) {
            pageIndex = Integer.parseInt(requestData.get("pageNum"));
        }
        if(requestData.get("pageSize")!=null) {
           Integer pageSize = Integer.parseInt(requestData.get("pageSize"));
            PageModel pageModel =new PageModel();
            pageModel.setPageSize(pageSize);
        }


        String keyValue = requestData.get("keyValue");

        session.setAttribute("keyValue", keyValue);


        JSONArray dataArray = new JSONArray();

        dataArray.add(userService.queryAllUserList(pageIndex,keyValue));
        return Result.success(dataArray);

    }
//    修改
    @RequestMapping("/userEdit")  // 标注访问地址
    @ResponseBody
    public HashMap userEdit(@RequestBody Map<String,String> requestData, HttpServletRequest request) {
        HashMap result2 = new HashMap();
        Integer userId = Integer.parseInt(requestData.get("userId"));
        result2.put("msg","");
        result2.put("error",0);
        HashMap userMap = new HashMap();
        userMap.put("user_data", userMapper.selectById(userId));
        userMap.put("depart_data", departMapper.selectList(null));

        result2.put("data",userMap);

        return result2;

    }
//    更新
    @RequestMapping("/userUpdate")  // 标注访问地址
    @ResponseBody
    public Result2 userUpdate(@RequestBody User requestData, HttpServletRequest request) {
        Result2 result = new Result2();

        int result_num = userMapper.updateUserByUserId(requestData);

        JSONArray dataArray = new JSONArray();
        dataArray.add(userService.queryAllUserList(1,""));
        result.setData(dataArray);
        if(result_num>0){
            result.setMsg("更新成功");
            result.setError(0);
        }else{
            result.setMsg("更新失败");
            result.setError(1);
        }
        return result;
    }
    //    更新
    @RequestMapping("/userUpdateByMobileAndUserName")  // 标注访问地址
    @ResponseBody
    public Result2 userUpdateByMobileAndUserName(@RequestBody User requestData, HttpServletRequest request) throws Exception {

        Result2 result = new Result2();
        String userName = RSAUtil.decrypt( requestData.getUserName());
        String userMobile = RSAUtil.decrypt( requestData.getUserMobile());

        User user = userMapper.queryeUserByUserNameAndMobile(userMobile, userName);
        if(user!=null){
            User newUser = new User();
            newUser.setUserName(userName);
            String userPwd = RSAUtil.decrypt( requestData.getUserPwd());
            newUser.setUserPwd(userPwd);
            newUser.setUserMobile(userMobile);
            int result_num  = userMapper.updateUserByUserNameAndMobile(newUser);
            if(result_num>0){
                result.setMsg("更新成功");
                result.setError(0);
            }else{
                result.setMsg("更新失败");
                result.setError(1);
            }
        }else{
            result.setMsg("用户不存在");
            result.setError(1);
        }

        return result;
    }

    @RequestMapping("/userUpdateByEmailAndUserName")  // 标注访问地址
    @ResponseBody
    public Result2 userUpdateByEmailAndUserName(@RequestBody User requestData, HttpServletRequest request) throws Exception {
        Result2 result = new Result2();
        String userName = RSAUtil.decrypt( requestData.getUserName());

        String useEmail = RSAUtil.decrypt( requestData.getUserEmail());

        User user = userMapper.queryeUserByUserNameAndEmail(useEmail, userName);
        if(user!=null){
            User newUser = new User();
            newUser.setUserName(userName);
            String userPwd = RSAUtil.decrypt( requestData.getUserPwd());
            newUser.setUserPwd(userPwd);
            newUser.setUserEmail(useEmail);
            int result_num  = userMapper.userUpdateByEmailAndUserName(newUser);
            if(result_num>0){
                result.setMsg("更新成功");
                result.setError(0);
            }else{
                result.setMsg("更新失败");
                result.setError(1);
            }
        }else{
            result.setMsg("用户不存在");
            result.setError(1);
        }

        return result;
    }
    //    增加
    @ResponseBody
    @RequestMapping("/userAdd")  // 标注访问地址

    public Result<JSONArray> userAdd(@RequestBody User user, HttpServletRequest request) {


        int result_num = userMapper.addUser(user.getUserName(), user.getUserPwd(),
                user.getUserMobile(), user.getUserEmail(),user.getUserSex());

        JSONArray dataArray = new JSONArray();
//        dataArray.add(userService.queryAllUserList(1,""));
        dataArray.add(result_num);
        return Result.success(dataArray);
    }
    //    更新
    @RequestMapping("/userDel")  // 标注访问地址
    @ResponseBody
    public Result<JSONArray> userDel(@RequestBody User user, HttpServletRequest request) {


        int result_num = userMapper.deleteUserByuserId(user.getUserId());

        JSONArray dataArray = new JSONArray();
        dataArray.add(userService.queryAllUserList(1,""));
        return Result.success(dataArray);
    }
}
