package com.example.express_admin.dao;

import java.util.ArrayList;
import java.util.HashMap;

public interface ElectronicComponentsDao {


    public HashMap queryAllComponentsList(Integer pageIndex, String keyValue);

    public HashMap queryAllComponentsOutOrInList(Integer pageIndex, String keyValue);

    public Integer componentsOutOrInList(ArrayList outOrInList);

    public HashMap checkApplication(String applicationId, Integer applicationStatusId);
    public HashMap queryApplication(String applicationId);

    public HashMap queryInventoryList(Integer pageIndex, String keyValue);
}



