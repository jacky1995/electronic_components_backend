package com.example.express_admin.dao;

import java.util.ArrayList;
import java.util.HashMap;

public interface PackageInfoDao {
	

    public HashMap queryAllSampleList(Integer pageIndex,String keyValue );
    public HashMap queryPackageList(Integer pageIndex,String keyValue ,Integer packageStatusId,Integer approveStatusId,Integer userId);


    public  HashMap queryLastInsertUnpayPackageInfo(Integer userId);

    public HashMap queryPackageBypackageNumber(String packageNumber);
    public Integer sendFetchEmail(ArrayList packageList);



    Integer sendWeChatNotice(ArrayList expressInfoMap);
    Integer updatePackagepackageStatusAndFetchCode(ArrayList expressInfoMap);

}
    
    

