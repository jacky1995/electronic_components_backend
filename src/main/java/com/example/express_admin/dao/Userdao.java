package com.example.express_admin.dao;

import com.example.express_admin.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;

public interface Userdao {


    public HashMap queryAllUserList(Integer pageIndex,String keyValue);
    public HashMap checkUserByUserNameAndPassWord(String user_name, String user_pwd);

    public User queryUserByUserId(Integer userId);
}
    
    

