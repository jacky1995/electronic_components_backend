package com.example.express_admin.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.example.express_admin.dao.BaseDao;
import com.example.express_admin.dao.ElectronicComponentsDao;

import com.example.express_admin.mapper.ElectronicComponentsMapper;

import com.example.express_admin.pojo.ElectronicComponentsApplication;
import com.example.express_admin.pojo.ElectronicComponentsModel;

import com.example.express_admin.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class ElectronicComponentsDaoImpl extends BaseDao implements ElectronicComponentsDao {
	ElectronicComponentsModel electronicComponentsModel = new ElectronicComponentsModel();
	ElectronicComponentsApplication electronicComponentsApplication = new ElectronicComponentsApplication();
	@Autowired
	private ElectronicComponentsMapper electronicComponentsMapper;
	@Autowired
	private SendEmailService sendEmailService;

	PageModel pageModel =new PageModel();
	AssembleSqlUtils assembleSqlUtils =new AssembleSqlUtils();
	HashMap resultMap = new HashMap<>();
	Page  page = new Page();

	public HashMap queryAllComponentsList(Integer pageIndex,String keyValue) {

		String  joinSql = "LEFT JOIN supplier s ON s.supplierId = ec.supplierId ";
		String baseSql = "SELECT * FROM electronic_components ec "+joinSql;
		String itemSql = "select count(*) from electronic_components ec "+joinSql;
		String pageSql = String.format(" limit %s,%s",(pageIndex-1)*pageModel.getPagesize(),pageModel.getPagesize());
		HashMap sqlMap = new HashMap();
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();
		if(keyValue!="" && keyValue !=null) {
			orConditionList.add(String.format(" s.supplierName like %s%s%s","'%",keyValue,"%'"));
			orConditionList.add(String.format(" ec.name like %s%s%s","'%",keyValue,"%'"));
			orConditionList.add(String.format(" ec.componentId like %s%s%s","'%",keyValue,"%'"));
			orConditionList.add(String.format(" ec.model like %s%s%s","'%",keyValue,"%'"));

		}
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);

		resultMap.put("data", queryForList(ElectronicComponentsModel.class, (String) sqlMap.get("itemsSql")));


		resultMap.put("page", page.getPage(pageIndex, (String) sqlMap.get("countSql")));
		return resultMap;


	}
	public HashMap queryAllComponentsOutOrInList(Integer pageIndex,String keyValue) {

		String joinSql = "LEFT JOIN t_user u ON a.userId = u.userId " +
				"LEFT JOIN electronic_components e_c ON e_c.componentId = a.componentId " +
				"LEFT JOIN application_status a_s ON a.applicationStatusId = a_s.applicationStatusId " +
				"LEFT JOIN supplier s ON s.supplierId = e_c.supplierId ";
		String baseSql = "SELECT * FROM application a "+joinSql;
		String itemSql = "select count(*) from application a "+joinSql;
		String pageSql = String.format(" limit %s,%s",(pageIndex-1)*pageModel.getPagesize(),pageModel.getPagesize());
		HashMap sqlMap = new HashMap();
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();
		if(keyValue!="" && keyValue !=null) {
			orConditionList.add(String.format(" u.userName like %s%s%s","'%",keyValue,"%'"));
			orConditionList.add(String.format(" s.supplierName like %s%s%s","'%",keyValue,"%'"));
			orConditionList.add(String.format(" e_c.name like %s%s%s","'%",keyValue,"%'"));
			orConditionList.add(String.format(" e_c.model like %s%s%s","'%",keyValue,"%'"));
			orConditionList.add(String.format(" e_c.componentId like %s%s%s","'%",keyValue,"%'"));

		}
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);

		resultMap.put("data", queryForList(ElectronicComponentsApplication.class, (String) sqlMap.get("itemsSql")));


		resultMap.put("page", page.getPage(pageIndex, (String) sqlMap.get("countSql")));
		return resultMap;


	}

	public HashMap queryInventoryList(Integer pageIndex,String keyValue) {

		ElectronicComponentsDaoImpl electronicComponentsDaoImpl = new ElectronicComponentsDaoImpl();


		HashMap resultMap2 = new HashMap<>();
		HashMap page = new HashMap<>();
		resultMap2.put("electronic_components",electronicComponentsDaoImpl.queryAllComponentsList(pageIndex,keyValue).get("data"));
		resultMap2.put("application",electronicComponentsDaoImpl.queryAllComponentsOutOrInList(pageIndex,keyValue).get("data"));
		page = (HashMap) electronicComponentsDaoImpl.queryAllComponentsOutOrInList(pageIndex,keyValue).get("page");
		resultMap2.put("page",page);
		resultMap.put("data",resultMap2);


		return resultMap;


	}
	public HashMap checkApplication(String componentId,Integer applicationStatusId) {
		HashMap userMap = new HashMap();

		try {
			electronicComponentsModel = electronicComponentsMapper.queryComponentByComponentId(componentId);
			List<ElectronicComponentsApplication> componentList = electronicComponentsMapper.queryapplicationByapplicationId(componentId);
			userMap.put("component_data",electronicComponentsModel);
			userMap.put("application_list",componentList);
		}catch (Exception e){
			userMap.put("component_data",null);
		}

		return userMap;


	}
//	public HashMap checkApplication(String componentId,Integer applicationStatusId) {
//		HashMap userMap = new HashMap();
//		System.out.println(componentId);
//		try {
//			if(applicationStatusId==0){//出库
//				electronicComponentsApplication = electronicComponentsMapper.queryapplicationByapplicationIdAndApplicationStatusId(componentId,1);
//				if(electronicComponentsApplication ==null){
//					electronicComponentsModel = null;
//				}else{
//					electronicComponentsModel = electronicComponentsMapper.queryComponentByComponentId(componentId);
//				}
//			}else{//入库
//				electronicComponentsApplication = electronicComponentsMapper.queryapplicationByapplicationIdAndApplicationStatusId(componentId,0);
//				if(electronicComponentsApplication==null){
//					electronicComponentsModel = null;
//				}else{
//					electronicComponentsModel = electronicComponentsMapper.queryComponentByComponentId(componentId);
//				}
//			}
//
//			userMap.put("component_data",electronicComponentsModel);
//		}catch (Exception e){
//			userMap.put("component_data",null);
//		}
//
//		return userMap;
//
//
//	}

	public HashMap queryApplication(String applicationId) {
		HashMap userMap = new HashMap();
		String[] parts = applicationId.split("_");
		try {
			String componentId = parts[0];
			electronicComponentsModel = electronicComponentsMapper.queryComponentByComponentId(componentId);
			userMap.put("component_data",electronicComponentsModel);
		}catch (Exception e){
			userMap.put("component_data",null);
		}

		return userMap;


	}
//	@Override
	public Integer componentsOutOrInList(ArrayList outOrInList) {
		int num = 0;
		for(Object outOrInObj:outOrInList) {
			HashMap JSONOutOrInObj = JSONObject.parseObject(JSONObject.toJSONString(outOrInObj),HashMap.class);
			Integer applicationStatusId = (Integer)JSONOutOrInObj.get("applicationStatusId");
			Integer newApplicationStatusId = applicationStatusId==0?1:0;
			String componentId = (String) JSONOutOrInObj.get("componentId");
			electronicComponentsMapper.queryapplicationByapplicationId(componentId);
			List<ElectronicComponentsApplication> componentList = electronicComponentsMapper.queryapplicationByapplicationId(componentId);

//				如果为空，则执行新增操作
			if(componentList.size()==0){
				num = electronicComponentsMapper.insertApplication(
						(Integer) JSONOutOrInObj.get("userId"),
						componentId,
						(Integer) JSONOutOrInObj.get("applicantNum"),
						applicationStatusId

				);

			}else{
				num = electronicComponentsMapper.updateComponentStatusByapplicationId(componentId,applicationStatusId);

			}

			int  quantity = applicationStatusId==1?1:-1;

			if(num!=0){
				electronicComponentsMapper.updateComponentQuantityByComponentId(componentId,quantity);

			}


		}
		return num;
	}
//@Override
//public Integer componentsOutOrInList(ArrayList outOrInList) {
//	int num = 0;
//	for(Object outOrInObj:outOrInList) {
//		HashMap JSONOutOrInObj = JSONObject.parseObject(JSONObject.toJSONString(outOrInObj),HashMap.class);
//		Integer applicationStatusId = (Integer)JSONOutOrInObj.get("applicationStatusId");
//		String componentId = (String) JSONOutOrInObj.get("componentId");
//		electronicComponentsApplication = electronicComponentsMapper.queryapplicationByapplicationIdAndApplicationStatusId(componentId,applicationStatusId);
//		num = electronicComponentsMapper.insertApplication(
//				(Integer) JSONOutOrInObj.get("userId"),
//				componentId,
//				(Integer) JSONOutOrInObj.get("applicantNum"),
//				applicationStatusId
//
//		);
//		int  quantity = applicationStatusId==1?1:-1;
//
//		if(num!=0){
//			electronicComponentsMapper.updateComponentQuantityByComponentId(componentId,quantity);
//
//		}
//
//
//	}
//	return num;
//}

	public static void main(String[] args) {

		ElectronicComponentsDaoImpl electronicComponentsDaoImpl = new ElectronicComponentsDaoImpl();

		System.out.println(electronicComponentsDaoImpl.queryInventoryList(1,""));
//        }
	}

}


