package com.example.express_admin.dao.impl;

import com.example.express_admin.dao.BaseDao;
import com.example.express_admin.dao.PackageInfoDao;
import com.example.express_admin.pojo.PackageType;
import com.example.express_admin.pojo.PackageInfo;
import com.example.express_admin.mapper.PackageMapper;
import com.example.express_admin.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;

@Component
public class PackageInfoDaoDaoImpl extends BaseDao implements PackageInfoDao {
	@Autowired
	private PackageMapper packageMapper;
	@Autowired
	private SendEmailService sendEmailService;

	PageModel pageModel =new PageModel();
	AssembleSqlUtils assembleSqlUtils =new AssembleSqlUtils();
	HashMap resultMap = new HashMap<>();
	Page  page = new Page();

	public HashMap queryAllSampleList(Integer pageIndex,String keyValue) {
		String baseSql = "SELECT * FROM sample";
		String itemSql = "select count(*) from sample";
		String pageSql = String.format(" limit %s,%s",(pageIndex-1)*pageModel.getPagesize(),pageModel.getPagesize());
		HashMap sqlMap = new HashMap();
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();
		if(keyValue!="" && keyValue !=null) {
			andConditionList.add(String.format(" user_name like %s%s%s","'%",keyValue,"%'"));
		}
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);

		resultMap.put("data", queryForList(PackageType.class, (String) sqlMap.get("itemsSql")));
		resultMap.put("page", page.getPage(pageIndex, (String) sqlMap.get("countSql")));
		return resultMap;


	}

	public HashMap queryPackageList1(Integer pageIndex,String keyValue,Integer packageStatusId,Integer approveStatusId,Integer userId) {
		String joinSql= " LEFT JOIN  express_company e_c on p.expressCompanyId = e_c.expressCompanyId LEFT JOIN package_approve_status p_s_a ON p_s_a.approveStatusId = p.approveStatusId " +
				"LEFT JOIN package_status p_s ON p_s.packageStatusId = p.packageStatusId " +
				"LEFT JOIN package_type t ON t.packageTypeId = p.packageTypeId " +
				"LEFT JOIN t_user u ON u.userId = p.userId "+
				"LEFT JOIN package_car c ON c.packageCarId = p.packageCarId ";
		String baseSql = "SELECT * FROM package_info p "+joinSql;
		String itemSql = "SELECT count(*)  FROM package_info p "+joinSql;
		String pageSql = String.format(" ORDER  BY p.packageTypeId limit %s,%s",(pageIndex-1)*pageModel.getPagesize(),pageModel.getPagesize());
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();

		if(keyValue!="" && keyValue !=null) {
			orConditionList.add(String.format(
//					" u.userName like %s%s%s  or t.packageTypeName LIKE %s%s%s or p.packageNumber LIKE %s%s%s or e_c.expressCompanyName LIKE %s%s%s ",
					" p.addresseeName like %s%s%s  or p.addresseeMobile LIKE %s%s%s or p.packageNumber LIKE %s%s%s or e_c.expressCompanyName LIKE %s%s%s ",
					"'%",keyValue,"%'",
					"'%",keyValue,"%'",
					"'%",keyValue,"%'",
					"'%",keyValue,"%'"


			));
		}
		if(packageStatusId!=null) {
			andConditionList.add(String.format(

					" p.packageStatusId = %s ",
					packageStatusId

			));
		}
		if(userId!=null) {
			andConditionList.add(String.format(
					" p.userId = %s ",
					userId

			));
		}
		if(approveStatusId!=null) {
			andConditionList.add(String.format(
					" p.approveStatusId = %s ",
					approveStatusId

			));
		}
		HashMap sqlMap = new HashMap();
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);
		System.out.println(sqlMap);
		resultMap.put("data", queryForList(PackageInfo.class, (String) sqlMap.get("itemsSql")));
		resultMap.put("page", page.getPage(pageIndex, (String) sqlMap.get("countSql")));

		return resultMap;


	}


	public HashMap queryPackageList(Integer pageIndex,String keyValue,Integer packageStatusId,Integer approveStatusId,Integer userId) {

		String joinSql= " LEFT JOIN  express_company e_c on p.expressCompanyId = e_c.expressCompanyId LEFT JOIN package_approve_status p_s_a ON p_s_a.approveStatusId = p.approveStatusId " +
				"LEFT JOIN package_status p_s ON p_s.packageStatusId = p.packageStatusId " +
				"LEFT JOIN package_type t ON t.packageTypeId = p.packageTypeId " +
				"LEFT JOIN t_user u ON u.userId = p.userId "+
				"LEFT JOIN package_car c ON c.packageCarId = p.packageCarId ";

		String baseSql = "SELECT * FROM package_info p "+joinSql;
		String itemSql = "SELECT count(*)  FROM package_info p "+joinSql;
		String pageSql = String.format(" ORDER  BY p.packageTypeId limit %s,%s",(pageIndex-1)*pageModel.getPagesize(),pageModel.getPagesize());
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();

		if(keyValue!="" && keyValue !=null) {
			orConditionList.add(String.format(
//					" u.userName like %s%s%s  or t.packageTypeName LIKE %s%s%s or p.packageNumber LIKE %s%s%s or e_c.expressCompanyName LIKE %s%s%s ",
					" p.addresseeName like %s%s%s  or p.addresseeMobile LIKE %s%s%s or p.packageNumber LIKE %s%s%s or e_c.expressCompanyName LIKE %s%s%s ",
					"'%",keyValue,"%'",
					"'%",keyValue,"%'",
					"'%",keyValue,"%'",
					"'%",keyValue,"%'"


			));
		}
		if(packageStatusId!=null) {
			andConditionList.add(String.format(

					" p.packageStatusId = %s ",
					packageStatusId

			));
		}
		if(userId!=null) {
			andConditionList.add(String.format(
					" p.userId = %s ",
					userId

			));
		}
		if(approveStatusId!=null) {
			andConditionList.add(String.format(
					" p.approveStatusId = %s ",
					approveStatusId

			));
		}
		HashMap sqlMap = new HashMap();
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);
		System.out.println(sqlMap);
		resultMap.put("data", queryForList(PackageInfo.class, (String) sqlMap.get("itemsSql")));
		resultMap.put("page", page.getPage(pageIndex, (String) sqlMap.get("countSql")));

		return resultMap;
	}


	public HashMap queryLastInsertUnpayPackageInfo(Integer userId) {
		String joinSql= " LEFT JOIN  express_company e_c on p.expressCompanyId = e_c.expressCompanyId LEFT JOIN package_approve_status p_s_a ON p_s_a.approveStatusId = p.approveStatusId LEFT JOIN package_status p_s ON p_s.packageStatusId = p.packageStatusId LEFT JOIN package_type t ON t.packageTypeId = p.packageTypeId LEFT JOIN t_user u ON u.userId = p.userId ";
		String baseSql = "SELECT * FROM package_info p "+joinSql;
		String itemSql = "SELECT count(*)  FROM package_info p "+joinSql;
		String pageSql = String.format(" ORDER BY p.packageId DESC limit 0,1");
		ArrayList andConditionList = new ArrayList<>();

		ArrayList orConditionList = new ArrayList<>();

		if(userId!=null) {
			andConditionList.add(String.format(
					" p.userId = %s ",
					userId

			));
		}

		andConditionList.add(String.format(
				" p.packageStatusId = %s ",
				5
		));

		HashMap sqlMap = new HashMap();
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);

		resultMap.put("data", queryForList(PackageInfo.class, (String) sqlMap.get("itemsSql")));
		resultMap.put("page", page.getPage(1, (String) sqlMap.get("countSql")));
		return resultMap;
	}


	public Integer sendFetchEmail(ArrayList packageList) {

		for (int i = 0; i < packageList.size(); i++) {
			HashMap jsonObject = (HashMap) packageList.get(i);
			String userEmail = (String) jsonObject.get("userEmail");
			String fetchCode = (String) jsonObject.get("fetchCode");
			String expressCompanyName = (String) jsonObject.get("expressCompanyName");
			String packageNumber = (String) jsonObject.get("packageNumber");

			String to = userEmail;
			String subject = "天天购驿站取件通知";
			String text = String.format("【天天购驿站】" +
							"您的%s包裹包裹%s已到菜鸟驿站，请凭取件码：%s取件。",
					expressCompanyName, packageNumber, fetchCode);
			sendEmailService.sendSimpleEmail(to, subject, text);
		}
		System.out.println("邮件全部发送完成");
		return 1;
	}

	public Integer updatePackagepackageStatusAndFetchCode(ArrayList expressInfoList) {

		for (int i = 0; i < expressInfoList.size(); i++) {
			HashMap jsonObject = (HashMap) expressInfoList.get(i);
			packageMapper.updatePackagepackageStatusAndFetchCode(
					(Integer) jsonObject.get("packageStatusId"),
					(Integer) jsonObject.get("packageId"),
					(String) jsonObject.get("fetchCode")
			);

		}
		return 1;
	}
	public Integer sendWeChatNotice(ArrayList expressInfoList) {
		XCXMain xCXMain = new XCXMain();
		for (int i = 0; i < expressInfoList.size(); i++) {
			HashMap jsonObject = (HashMap) expressInfoList.get(i);
			xCXMain.sendMessage(jsonObject);
		}
		System.out.println("公众号通知发送完成");
		return 1;
	}

	@Override
	public HashMap queryPackageBypackageNumber(String packageNumber) {
		String joinSql= " LEFT JOIN  express_company e_c on p.expressCompanyId = e_c.expressCompanyId LEFT JOIN package_approve_status p_s_a ON p_s_a.approveStatusId = p.approveStatusId " +
				"LEFT JOIN package_status p_s ON p_s.packageStatusId = p.packageStatusId " +
				"LEFT JOIN package_type t ON t.packageTypeId = p.packageTypeId " +
				"LEFT JOIN t_user u ON u.userId = p.userId "+
				"LEFT JOIN package_car c ON c.packageCarId = p.packageCarId ";

		String baseSql = "SELECT * FROM package_info p "+joinSql;
		String itemSql = "SELECT count(*)  FROM package_info p "+joinSql;
		String pageSql = " ORDER  BY p.packageTypeId limit 0,1";
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();

		orConditionList.add(String.format(" p.packageNumber='%s'",
				packageNumber


		));

		HashMap sqlMap = new HashMap();
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);
		System.out.println(sqlMap);
		resultMap.put("data", queryForList(PackageInfo.class, (String) sqlMap.get("itemsSql")));
		resultMap.put("page", page.getPage(1, (String) sqlMap.get("countSql")));

		return resultMap;
	}


	public static void main(String[] args)  {
		PackageInfoDaoDaoImpl packageDaoImpl = new PackageInfoDaoDaoImpl();
		XCXMain xCXMain = new XCXMain();
		HashMap expressInfoMap = new HashMap();
		ArrayList expressInfoList = new ArrayList();
		expressInfoMap.put("expressCompanyName","圆通");
		expressInfoMap.put("packageNumber","123");
		expressInfoMap.put("fetchCode","9527");
		expressInfoMap.put("address","松田");
		expressInfoList.add(expressInfoMap);
		packageDaoImpl.sendWeChatNotice(expressInfoList);
//		String[] out_trade_no_list ="1682567604114_1_1".split("_");
//		Integer packageId = Integer.valueOf(out_trade_no_list[out_trade_no_list.length-1]);

//		System.out.println(packageDaoImpl.queryPackageBypackageNumber("YT8700036560985"));
//		System.out.println(packageDaoImpl.queryLastInsertUnpayPackageInfo(1));
//		System.out.println(packageDaoImpl.queryPackageList(1,"",null,null,1));

//		System.out.println(packageDaoImpl.queryPackageList(1,"",2,1,1));

	}
}


