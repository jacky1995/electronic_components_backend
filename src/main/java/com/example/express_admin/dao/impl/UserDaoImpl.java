package com.example.express_admin.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.express_admin.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.express_admin.dao.BaseDao;
import com.example.express_admin.dao.Userdao;
import com.example.express_admin.pojo.User;
import com.example.express_admin.utils.Page;
import com.example.express_admin.utils.AssembleSqlUtils;
import com.example.express_admin.utils.PageModel;
import org.springframework.stereotype.Service;

@Service

public class UserDaoImpl extends BaseDao implements Userdao{

	@Autowired
	private UserMapper userMapper;
	PageModel pageModel =new PageModel();
	AssembleSqlUtils assembleSqlUtils =new AssembleSqlUtils();
	HashMap resultMap = new HashMap<>();
	Page  page = new Page();

	public HashMap queryAllUserList(Integer pageIndex,String keyValue) {


		String baseSql = "SELECT * FROM t_user u";
		String itemSql = "select count(*) from t_user u";
		String pageSql = String.format(" limit %s,%s",(pageIndex-1)*pageModel.getPagesize(),pageModel.getPagesize());
		HashMap sqlMap = new HashMap();
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();

		if(keyValue!="" && keyValue !=null) {
			orConditionList.add(String.format(

					" u.userName like %s%s%s or u.userSex like %s%s%s",
					"'%",keyValue,"%'","'%",keyValue,"%'"

			));
		}
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);

		resultMap.put("data", queryForList(User.class, (String) sqlMap.get("itemsSql")));
		resultMap.put("page", page.getPage(pageIndex, (String) sqlMap.get("countSql")));

		return resultMap;


	}


	//	通过用户名和密码查询用户

	public HashMap checkUserByUserNameAndPassWord(String user_name, String user_pwd) {

		String baseSql = "SELECT * FROM t_user u LEFT JOIN t_depart d ON u.departId = d.departId ";
		String itemSql = "select count(*) from t_user u LEFT JOIN t_depart d ON u.departId = d.departId ";
		String pageSql = String.format(" limit 0,1");
		HashMap sqlMap = new HashMap();
		ArrayList andConditionList = new ArrayList<>();
		ArrayList orConditionList = new ArrayList<>();
		andConditionList.add(String.format(" userName = '%s' ",user_name));
		andConditionList.add(String.format(" userPwd = '%s' ",user_pwd));
		sqlMap = assembleSqlUtils.assembleWhereSql(andConditionList,orConditionList,baseSql,itemSql,pageSql);

		resultMap.put("data", queryForList(User.class, (String) sqlMap.get("itemsSql")));
		resultMap.put("page", page.getPage(1, (String) sqlMap.get("countSql")));
		return resultMap;
	}


	public User queryUserByUserId(Integer userID) {
		System.out.println(userMapper);
		return  userMapper.queryUserByUserId(userID);
	}


	public static void main(String[] args)  {

		UserDaoImpl  userDaoImpl = new UserDaoImpl();
//
////		System.out.println(userDaoImpl.queryAllUserList(1,"张学友"));
//		UserDaoImpl userRepository = null;
//		System.out.println(userDaoImpl.queryUserByUserId(1));
		System.out.println(userDaoImpl.checkUserByUserNameAndPassWord("admin","Aa888888"));

	}
}


