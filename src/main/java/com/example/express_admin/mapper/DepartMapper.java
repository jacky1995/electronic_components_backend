package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.express_admin.pojo.Depart;
import com.example.express_admin.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@TableName("t_depart")
@Mapper
public interface DepartMapper  extends BaseMapper<User> {


	//	查询部门
	@Select("select * from t_depart")
	public List<Depart> queryAllDepart();



}
