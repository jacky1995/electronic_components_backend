package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.example.express_admin.pojo.ElectronicComponentsApplication;
import com.example.express_admin.pojo.ElectronicComponentsModel;
import com.example.express_admin.pojo.ElectronicLoop;
import org.apache.ibatis.annotations.*;

import java.util.List;

@TableName("electronic_components")
@Mapper
public interface ElectronicComponentsMapper extends BaseMapper<ElectronicComponentsModel> {
    @Delete("delete  from electronic_components  WHERE componentId =#{componentId}")
    public int delComponentNum(String componentId);
    @Select("select count(*) from electronic_components ")
    public int queryComponentNum();
    @Select("select * from electronic_components e_c  " +
            "LEFT JOIN supplier s ON s.supplierId = e_c.supplierId " +
            "WHERE e_c.componentId =#{componentId}")
    public ElectronicComponentsModel queryComponentByComponentId(String componentId);
    @Select("select * from electronic_components e_c  " +
            "LEFT JOIN supplier s ON s.supplierId = e_c.supplierId " +
            "WHERE e_c.componentId =#{componentId} and e_c.applicationStatusId = #{applicationStatusId}")
    public ElectronicComponentsModel queryComponentByComponentIdAndApplicationStatusId(String componentId,Integer applicationStatusId);
    @Insert("INSERT electronic_components " +
            "(componentId ,name,model,imageUrl," +
            "specification,serialNumber,description,supplierId,price,quantity) " +
            "values(#{componentId},#{name} ,#{model}," +
            "#{imageUrl},#{specification},#{serialNumber},#{description},#{supplierId}," +
            "#{price},#{quantity})")
    public Integer insertComponent(
            String componentId, String name,
            String model, String imageUrl,
            String specification, String serialNumber, String description, Integer supplierId,
            String price,
            Integer quantity);

    @Update("UPDATE electronic_components " +
            "SET componentId = #{componentId} , name = #{name} , model = #{model} ,  imageUrl = #{componentImageUrl} , specification = #{specification} ," +
            " serialNumber = #{serialNumber},description=#{description},supplierId = #{supplierId} ," +
            "price = #{price}, quantity = #{quantity}" +
            " WHERE componentId =#{oldComponentId}")
    public Integer updateComponentByComponentId(String oldComponentId,
                                                String componentId, String name,
            String model,
            String specification, String serialNumber, String description, Integer supplierId,
            String price,
            String componentImageUrl,Integer quantity);


//    出入库申请
    @Insert("INSERT application " +
            "( userId,componentId,applicantNum,applicationStatusId) " +
            "values(#{userId},#{componentId},#{applicantNum},#{applicationStatusId})")
    public int insertApplication( Integer userId,String componentId, Integer applicantNum, Integer applicationStatusId);


//    更新库存量
    @Update("UPDATE electronic_components " +
            "SET quantity = quantity + #{num}" +
            " WHERE componentId =#{componentId}")
    public Integer updateComponentQuantityByComponentId(
            String componentId, Integer num);

    //    更新申请记录状态
    @Update("UPDATE application " +
            "SET applicationStatusId =#{applicationStatusId}" +
            " WHERE componentId =#{componentId}")
    public Integer updateComponentStatusByapplicationId(
            String componentId,Integer applicationStatusId);


    //根据applicationId查询申请记录

    @Select("select * from application  WHERE componentId =#{componentId} and applicationStatusId=#{applicationStatusId}")
    public ElectronicComponentsApplication queryapplicationByapplicationIdAndApplicationStatusId(
            String componentId,Integer applicationStatusId);

    @Select("select * from application  WHERE componentId =#{componentId}")
    public List<ElectronicComponentsApplication> queryapplicationByapplicationId(
            String componentId);
    @Select("select * from application  WHERE componentId =#{componentId} and applicationStatusId=#{applicationStatusId}")
    public List<ElectronicComponentsApplication> queryapplicationByapplicationIdAndComponentId(
            String componentId,Integer applicationStatusId);
    @Insert("INSERT ignore supplier " +
            "(supplierName) " +
            "values(#{supplierName})")
    public int insertSupplier(String supplierName);

    @Select("select * from electronic_loop")
    public List<ElectronicLoop> queryComponentsLoop();

}
