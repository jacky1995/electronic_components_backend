package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.express_admin.pojo.ExpressCompany;
import com.example.express_admin.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
@TableName("express_company")
@Mapper
public interface ExpressCompanyMapper extends BaseMapper<ExpressCompany> {


}
