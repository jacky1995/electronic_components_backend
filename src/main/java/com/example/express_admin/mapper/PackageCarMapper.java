package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.express_admin.pojo.PackageType;
import org.apache.ibatis.annotations.Mapper;

@TableName("package_car")
@Mapper
public interface PackageCarMapper extends BaseMapper<PackageType> {
;


}
