package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.express_admin.pojo.*;
import org.apache.ibatis.annotations.*;

import java.util.List;
@TableName("package_Info")
@Mapper
public interface PackageMapper extends BaseMapper<PackageInfo> {
////	查询数据操作
//	@Select("select user_id,user_pwd,user_name from t_user WHERE user_name =#{userName} and user_pwd  = #{userPwd}")
//	public Sample queryAllSampleList(@Param("userName") String userName, @Param("userPwd") String userPwd);


	//	查询数据操作
	@Select("select * from package_type")
	public List<PackageType> queryAllPackageTypeList();

	//	查询数据操作
	@Select("select * from package_car")
	public List<PackageCar> queryAllPackageCarList();
	//	查询数据操作
	@Select("select * from package_status")
	public List<PackageStatus> queryAllPackageStatusList();

	//	通过型号ID更新
	@Update("UPDATE package_Info " +
			"SET packageId = #{packageId} , expressCompanyId = #{expressCompanyId} ,  packageName = #{packageName} , packageNumber = #{packageNumber} ," +
			" packageOperatorId = #{packageOperatorId},packageStatusId=#{packageStatusId},addresseeMobile = #{addresseeMobile} ," +
			"addresseeAddress = #{addresseeAddress} ,addresseeName = #{addresseeName} , packageTypeId = #{packageTypeId}, packageImageUrl = #{packageImageUrl} " +
			" , packageWeight = #{packageWeight}, expressCosts = #{expressCosts}  " +
			" WHERE packageId =#{packageId}")
	public Integer updatepackageBypackageId(
			Integer packageId,Integer expressCompanyId,String packageName,String packageNumber,
			Integer packageOperatorId,Integer packageStatusId,String addresseeMobile,
			String  addresseeAddress,String  addresseeName,Integer packageTypeId,String packageImageUrl,
			String packageWeight,
			String expressCosts



	);
	@Update("UPDATE package_Info " +
			"SET packageStatusId=#{packageStatusId},tradeNo=#{tradeNo}  WHERE packageId =#{packageId}")
	public Integer updatePackageOrderStatusIdBypackageId(Integer packageStatusId,Integer packageId,String tradeNo);
	//	通过型号ID更新包裹状态
	@Update("UPDATE package_Info " +
			"SET packageStatusId=#{packageStatusId}  WHERE packageId =#{packageId}")
	public Integer updatePackageStatusIdBypackageId(Integer packageStatusId,Integer packageId);
	//	通过型号ID更新
	@Update("UPDATE package_info " +
			"SET approveStatusId = #{approveStatusId} " +
					" WHERE packageId =#{packageId}")
	public Integer updateSampleModelApproveStatusBySampleModelId(
												@Param("sampleModelId") Integer sampleModelId,
												@Param("approveStatusId") Integer approveStatusId


	);
//	新增
	@Insert("INSERT package_info " +
			"(expressCompanyId , packageName  ,packageStatusId,addresseeMobile," +
			"addresseeAddress,addresseeName,packageTypeId,packageImageUrl,packageWeight,expressCosts,userId) " +
			"values(#{expressCompanyId},#{packageName} ,#{packageStatusId}," +
			"#{addresseeMobile},#{addresseeAddress},#{addresseeName},#{packageTypeId},#{packageImageUrl}," +
			"#{packageWeight},#{expressCosts},#{userId})")
	public Integer insertPackage(
			Integer expressCompanyId,String packageName,
			Integer packageStatusId,String addresseeMobile,
			String  addresseeAddress,String  addresseeName,Integer packageTypeId,String packageImageUrl,
			String packageWeight,String expressCosts,Integer userId


	);



	@Select("SELECT * FROM package_info p LEFT JOIN  express_company e_c on p.expressCompanyId = e_c.expressCompanyId LEFT JOIN package_approve_status p_s_a ON p_s_a.approveStatusId = p.approveStatusId LEFT JOIN package_status p_s ON p_s.packageStatusId = p.packageStatusId LEFT JOIN package_type t ON t.packageTypeId = p.packageTypeId LEFT JOIN t_user u ON u.userId = p.addresseeName LEFT JOIN package_car c ON c.packageCarId = p.packageCarId WHERE packageId =#{packageId}")
	public PackageInfo queryPackageByPackageId(@Param("packageId") Integer packageId);
	//	通过型号ID删除
	@Delete("delete  from package_info  WHERE packageId =#{packageId}")
	public Integer deletePackageByModelId(@Param("packageId") Integer packageId);

	@Select("select * from package_cabinet")
	public String findCabinetData();


	@Update("UPDATE package_Info set packageStatusId = 9 WHERE packageId in (#{packageIdStr})")
	public Integer packageInStore(String  packageIdStr);

	@Update("UPDATE package_Info set packageStatusId = 10 WHERE packageId in (#{packageIdStr})")
	public Integer packageOutStore(String  packageIdStr);

	@Update("UPDATE package_cabinet set cabinetData = #{cabinetData}")
	public Integer updatePackageCabinetData(String  cabinetData);
	@Update("UPDATE package_Info set packageStatusId = #{packageStatusId} WHERE packageId in (#{packageIdStr})")
	public Integer updatePackagepackageStatus(Integer  packageStatusId,String  packageIdStr);
	@Update("UPDATE package_Info set packageStatusId = #{packageStatusId},fetchCode=#{fetchCode} WHERE packageId in (#{packageId})")
	public Integer updatePackagepackageStatusAndFetchCode(Integer  packageStatusId,Integer  packageId,String fetchCode);
}
