package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.express_admin.pojo.PackageStatus;
import com.example.express_admin.pojo.PackageType;
import org.apache.ibatis.annotations.Mapper;

@TableName("package_status")
@Mapper
public interface PackageStatusMapper extends BaseMapper<PackageStatus> {
;


}
