package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.express_admin.pojo.PackageCar;
import com.example.express_admin.pojo.PackageInfo;
import com.example.express_admin.pojo.PackageStatus;
import com.example.express_admin.pojo.PackageType;
import org.apache.ibatis.annotations.*;

import java.util.List;

@TableName("package_type")
@Mapper
public interface PackageTypeMapper extends BaseMapper<PackageType> {
;


}
