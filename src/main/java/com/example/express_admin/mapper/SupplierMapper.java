package com.example.express_admin.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.example.express_admin.pojo.supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;
import java.util.List;

@TableName("electronic_components")
@Mapper
public interface SupplierMapper extends BaseMapper<supplier> {
    @Select("select * from supplier")
    public List<supplier> querySupplierList();


}
