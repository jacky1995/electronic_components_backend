package com.example.express_admin.mapper;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import com.example.express_admin.pojo.User;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    //	通过用户名和密码查询
    @Select("select * from t_user u LEFT JOIN t_depart d ON u.departId = d.departId WHERE u.userName =#{userName} and u.userPwd  = #{userPwd}")
    public User queryUserByUserNameAndPassWord(@Param("userName") String userName, @Param("userPwd") String userPwd);
    //	通过用户名和密码查询
    @Select("select * from t_user u LEFT JOIN t_depart d ON u.departId = d.departId WHERE u.userName =#{userName}")
    public User queryUserByUserName(@Param("userName") String userName);

    //	通过用户ID查询

    @Select("select * from t_user u LEFT JOIN t_depart d ON u.departId = d.departId WHERE u.userId =#{userId}")
    public User queryUserByUserId(@Param("userId") Integer userId);
    //	通过用户ID更新
    @Update("UPDATE t_user " +
            "SET  userName = #{userName} , userPwd = #{userPwd} ," +
            " departId = #{departId}, userMobile = #{userMobile} ,userEmail = #{userEmail} " +
            "WHERE userId =#{userId}")
    public Integer updateUserByUserId(User user);

    //	通过用户名和手机号修改密码
    @Update("UPDATE t_user " +
            "SET  userPwd = #{userPwd} WHERE userName =#{userName} and userMobile =#{userMobile}")
    public Integer updateUserByUserNameAndMobile(User user);

    @Update("UPDATE t_user " +
            "SET  userPwd = #{userPwd} WHERE userName =#{userName} and userMobile =#{userMobile} and userPwd =#{userPwd}")
    public Integer updateUserByUserNameAndMobileAndpwd(User user);
    @Update("UPDATE t_user " +
            "SET  userPwd = #{userPwd} WHERE userId =#{userId}")
    public Integer updateUserPwdByUserId(User user);


    //	通过用户名和邮箱修改密码
    @Update("UPDATE t_user " +
            "SET  userPwd = #{userPwd} WHERE userName =#{userName} and userEmail =#{userEmail}")
    public Integer userUpdateByEmailAndUserName(User user);


    //	通过用户名和手机号查找
    @Select("SELECT * FROM t_user WHERE userName =#{userName} and userMobile =#{userMobile}")
    public User queryeUserByUserNameAndMobile(

            @Param("userMobile") String userMobile,
            @Param("userName") String userName);



    //	通过用户名和邮箱查找
    @Select("SELECT * FROM t_user WHERE userName =#{userName} and userEmail =#{userEmail}")
    public User queryeUserByUserNameAndEmail(

            @Param("userEmail") String userEmail,
            @Param("userName") String userName);
    @Insert("INSERT t_user " +
            "(userName , userPwd , userMobile ,userEmail,userSex) " +
            "values(#{userName},#{userPwd} , #{userMobile},#{userEmail},#{userSex})")
    public Integer addUser(@Param("userName") String userName, @Param("userPwd") String userPwd,
                           @Param("userMobile") String userMobile,
                           @Param("userEmail") String userEmail,
                           @Param("userSex") String userSex
    );
    //	通过用户ID删除
    @Delete("delete  from t_user  WHERE userId =#{userId}")
    public Integer deleteUserByuserId(@Param("userId") Integer userId);

    //	查询所有
    @Select("select * from t_user u LEFT JOIN t_depart d ON u.departId = d.departId order by u.departId ")
    public List<User> queryAllUserList();
    //	分页查询
    @Select("select * from t_user u LEFT JOIN t_depart d ON u.departId = d.departId limit #{begin},#{end}")
    public List<User> queryUserListByPage(@Param("begin") Integer begin, @Param("end") Integer end);
    //	分页和精准查询

    @Select("select * from t_user u " +
            "LEFT JOIN t_depart d ON u.departId = d.departId " +
            "WHERE " +
            "u.userName like CONCAT('%',#{keyValue},'%') or u.userSex like CONCAT('%',#{keyValue},'%')  or d.departName like CONCAT('%',#{keyValue},'%') " +
            "limit 0,5"
    )
    public List<User> queryUserListByPageAndKey(@Param("begin") Integer begin, @Param("end") Integer end,@Param("keyValue") String keyValue);


}
