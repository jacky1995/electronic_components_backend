package com.example.express_admin.pojo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Data
@Document(collection ="t_depart")
@Component
@Repository
public class Depart {

	private Integer departId;
	private String departName;


}

