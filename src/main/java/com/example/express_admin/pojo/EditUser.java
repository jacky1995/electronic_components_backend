package com.example.express_admin.pojo;

import lombok.Data;

import java.util.List;

/**
 * @PACKAGE_NAME: com.example.express_admin.pojo
 * @NAME: EditUser
 * @USER: jacky
 * @DATE: 2023/3/24
 * @TIME: 12:41
 * @YEAR: 2023
 * @MONTH: 03
 * @MONTH_NAME_SHORT: 3月
 * @MONTH_NAME_FULL: 三月
 * @DAY: 24
 * @DAY_NAME_SHORT: 周五
 * @DAY_NAME_FULL: 星期五
 * @HOUR: 12
 * @MINUTE: 41
 * @PROJECT_NAME: store-admin-system
 */
@Data
public class EditUser {

    private Integer departId;
    private String departName;
    private List<Depart> depart_group;
    private String userEmail;
    private Integer userId;
    private String userMobile;
    private String userName;
    private String userPwd;
    private String userSex;



}
