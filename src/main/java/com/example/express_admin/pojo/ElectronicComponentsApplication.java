package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ElectronicComponentsApplication{
    @TableId("componentId")
    private String componentId;
    private String componentIdNum;
    private int applicationStatusId;
    private String fetchCode;
    private Timestamp applicationTime;
    private String applicationStatus;
//    用户信息部分

    private int userId;
    private String userName;

    //元器件部分
    private String name;
    private String model;
    private String imageUrl;
    private String specification;
    private String serialNumber;
    private String supplierName;
    private String description;
    private int supplierId;
    private String price;
    private int quantity;



}

