package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ElectronicComponentsModel {
    @TableId("componentId")
    private String componentId;

    private String name;


    private String model;

    private String imageUrl;


    private String specification;


    private String serialNumber;
    private String supplierName;


    private String description;

    private int supplierId;
    private int quantity;
    private String price;
}

