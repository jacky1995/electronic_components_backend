package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class ElectronicLoop {
    @TableId("loopId")
    private int loopId;
    private String imageUrl;

}

