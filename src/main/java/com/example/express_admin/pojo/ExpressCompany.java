package com.example.express_admin.pojo;

import lombok.Data;

@Data
public class ExpressCompany {
	private int expressCompanyId;
	private String expressCompanyName;
	private String expressCompanyCode;


}

