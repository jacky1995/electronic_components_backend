package com.example.express_admin.pojo;

import lombok.Data;

@Data
public class Inventory {
    private int id;
    private int componentId;
    private int quantity;
    private int warningLevel;
    private int max_warningLevel;
    private int min_warningLevel;
}

