package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class PackageCar {
	@TableId("packageCarId")
	private int packageCarId;
	private String packageCarNumber;
	private String packageCarType;
	private int packageCarWeight;





}

