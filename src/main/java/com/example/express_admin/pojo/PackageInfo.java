package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class PackageInfo {
	private Integer userId;
	@TableId("packageId")
	private int packageId;
	private String packageName;
	private int packageTypeId;
	private int packageOperatorId;
	private int packageStatusId;
	private String packageStatus;
	private int approveStatusId;
	private String packageImageUrl;
	private String addresseeName;
	private String addresseeAddress;
	private String addresseeMobile;
	private String packageNumber;
	private String packageTypeName;
	private int expressCompanyId;
	private String expressCompanyName;
	private String expressCompanyCode;
	private Timestamp createTime;
	private String packageWeight;
	private String expressCosts;
	private String tradeNo;
	private int packageCarId;
	private String packageCarNumber;
	private String packageCarType;
	private int packageCarWeight;
	private String userEmail;
	private String fetchCode;
	private String openId;
}

