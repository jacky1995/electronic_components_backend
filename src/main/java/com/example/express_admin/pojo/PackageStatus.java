package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class PackageStatus {
	@TableId("packageStatusId")
	private int packageStatusId;
	private String packageStatus;


}

