package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.security.Timestamp;

@Data
public class PurchaseOrders {
    @TableId("orderNumber")
    private int orderNumber;
    private int orderStatusId;
    private int supplierId;
    private Timestamp orderDate;
    private Timestamp deliveryDate;
    private String recipient;
    private String deliveryAddress;
}

