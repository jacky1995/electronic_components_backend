package com.example.express_admin.pojo;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

/**
 * @PACKAGE_NAME: com.example.express_admin.pojo
 * @NAME: Result
 * @USER: jacky
 * @DATE: 2023/3/16
 * @TIME: 12:32
 * @YEAR: 2023
 * @MONTH: 03
 * @MONTH_NAME_SHORT: 3月
 * @MONTH_NAME_FULL: 三月
 * @DAY: 16
 * @DAY_NAME_SHORT: 周四
 * @DAY_NAME_FULL: 星期四
 * @HOUR: 12
 * @MINUTE: 32
 * @PROJECT_NAME: store-admin-system
 */
@Data
public class Result2 {
    int error;
    String msg;
    JSONArray data = new JSONArray();

}
