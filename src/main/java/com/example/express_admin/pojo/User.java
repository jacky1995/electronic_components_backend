package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


import org.springframework.data.annotation.Id;


@Data

@TableName(value="t_user")

public class User {
	@TableId("userId")
	private Integer userId;
	private String userPwd;
	private String userName;
	private String userSex;
	private Integer departId;
	private String userMobile;
	private String userEmail;
	private String openId;
	private Integer sid;



}

