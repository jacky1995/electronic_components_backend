package com.example.express_admin.pojo;

import lombok.Data;

/**
 * @PACKAGE_NAME: com.example.express_admin.pojo
 * @NAME: WXMessage
 * @USER: jacky
 * @DATE: 2023/5/6
 * @TIME: 12:05
 * @YEAR: 2023
 * @MONTH: 05
 * @MONTH_NAME_SHORT: 5月
 * @MONTH_NAME_FULL: 五月
 * @DAY: 06
 * @DAY_NAME_SHORT: 周六
 * @DAY_NAME_FULL: 星期六
 * @HOUR: 12
 * @MINUTE: 05
 * @PROJECT_NAME: express-admin-system
 */
@Data
public class WeChatTemplateMsg {
    /**
     * 消息
     */
    private String value;
    /**
     * 消息颜色
     */
    private String color;


    public WeChatTemplateMsg(String value) {
        this.value = value;
        this.color = "#173177";
    }

    public WeChatTemplateMsg(String value, String color) {
        this.value = value;
        this.color = color;
    }

////    快递名称：{{expressCompanyName.DATA}}
//快递单号：{{packageNumber.DATA}}
//取件码：{{fetchCode.DATA}}
//取件地点:{{address.DATA}}

//    private String expressCompanyName;
//    private Integer packageNumber;
//    private String fetchCode;
//    private String address;
    /**
     * 消息颜色
     */
//    private String color = "#173177";




}
