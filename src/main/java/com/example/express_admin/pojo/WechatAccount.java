package com.example.express_admin.pojo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
//微信接口测试号数据
public class WechatAccount {
    private String mpAppId = "wx213670c2f96d09cd";
    private String templateId = "5rn78pFnPoUJHwWf9fQ3JGvAhK6iAyTrFxWkb94AC8w";
    private String mpAppSecret = "93f8ed7ed672719ce797ddf748de97ac";
}