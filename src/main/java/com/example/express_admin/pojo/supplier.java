package com.example.express_admin.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class supplier {
    @TableId("supplierId")
    private int supplierId;
    private String supplierName;

}

