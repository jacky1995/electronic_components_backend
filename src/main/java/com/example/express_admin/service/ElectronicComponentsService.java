package com.example.express_admin.service;

import java.util.ArrayList;
import java.util.HashMap;


public interface ElectronicComponentsService {

    public HashMap queryAllComponentsList(Integer pageIndex,String keyValue);//查询所有元器件列表
    public Integer componentsOutOrInList(ArrayList outOrInList);//出入库列表操作
    public HashMap queryAllComponentsOutOrInList(Integer pageIndex,String keyValue);//查询所有元器件出入库列表


    public HashMap checkApplication(String applicationId, Integer applicationStatusId);
    public HashMap queryApplication(String applicationId);
    public HashMap queryInventoryList(Integer pageIndex, String keyValue);
}
