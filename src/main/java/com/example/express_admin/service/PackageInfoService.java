package com.example.express_admin.service;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public interface PackageInfoService {

    public HashMap queryAllSampleList(Integer pageIndex,String keyValue);//查询所有样品列表
    public HashMap queryPackageList(Integer pageIndex,String keyValue,Integer packageStatusId,Integer approveStatusId,Integer userId);//查询所有样品列表
    public HashMap queryPackageBypackageNumber(String packageNumber);//通过单号查询
    public HashMap queryLastInsertUnpayPackageInfo(Integer userId);//查询所有样品列表
    JSONObject getPackagePost(String packageNumber, String expressCompanyCode);
    Integer sendFetchEmail(ArrayList packageList);
    Integer sendWeChatNotice(ArrayList expressInfoMap);



    int updatePackagepackageStatusAndFetchCode(ArrayList expressInfoMap);
}
