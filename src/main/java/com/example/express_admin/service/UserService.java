package com.example.express_admin.service;

import com.example.express_admin.pojo.User;

import java.util.HashMap;


public interface UserService {

    public HashMap queryAllUserList(Integer pageIndex,String keyValue);//查询所有用户列表

    //	通过用户名和密码查询用户
    public HashMap checkUserByUserNameAndPassWord(String user_name, String user_pwd);

    User queryUserByUserId(Integer userId);
}
