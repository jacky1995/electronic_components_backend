package com.example.express_admin.service.impl;


import com.example.express_admin.dao.ElectronicComponentsDao;

import com.example.express_admin.service.ElectronicComponentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class ElectronicComponentsServiceImpl implements ElectronicComponentsService {

	@Autowired
	 private ElectronicComponentsDao electronicComponentsDao;


	@Override
	public HashMap queryAllComponentsList(Integer pageIndex,String keyValue ) {

		return  electronicComponentsDao.queryAllComponentsList(pageIndex,keyValue);

	}

	@Override
	public Integer componentsOutOrInList(ArrayList outOrInList) {
		return  electronicComponentsDao.componentsOutOrInList(outOrInList);

	}

	@Override
	public HashMap queryAllComponentsOutOrInList(Integer pageIndex,String keyValue ) {

		return  electronicComponentsDao.queryAllComponentsOutOrInList(pageIndex,keyValue);

	}

	@Override
	public HashMap checkApplication(String applicationId, Integer applicationStatusId) {
		return  electronicComponentsDao.checkApplication(applicationId,applicationStatusId);
	}
	@Override
	public HashMap queryApplication(String applicationId) {
		return  electronicComponentsDao.queryApplication(applicationId);
	}

	@Override
	public HashMap queryInventoryList(Integer pageIndex, String keyValue) {
		return  electronicComponentsDao.queryInventoryList(pageIndex,keyValue);
	}


	public static void main(String[] args) {

//		PackageInfoDao packageDao = new PackageInfoDaoDaoImpl();
//		packageDao.queryAllSampleList(1,"张三");
//		System.out.println(packageDao.getPackagePost("1","12"));
//        }
	}
}
