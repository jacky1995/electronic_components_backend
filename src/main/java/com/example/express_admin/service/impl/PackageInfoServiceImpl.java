package com.example.express_admin.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.express_admin.dao.PackageInfoDao;
import com.example.express_admin.dao.impl.PackageInfoDaoDaoImpl;
import com.example.express_admin.service.PackageInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.express_admin.utils.KdApiSearchDemo;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class PackageInfoServiceImpl implements PackageInfoService {


	 private PackageInfoDao packageDao;

	@Autowired
	public PackageInfoServiceImpl(PackageInfoDaoDaoImpl packageDao) {
		this.packageDao = packageDao;
	}

	@Override
	public HashMap queryAllSampleList(Integer pageIndex,String keyValue ) {

		return  packageDao.queryAllSampleList( pageIndex,keyValue);

	}

	@Override
	public HashMap queryPackageList(Integer pageIndex, String keyValue,Integer packageStatusId,Integer approveStatusId,Integer userId) {
		return packageDao.queryPackageList( pageIndex,keyValue,packageStatusId,approveStatusId,userId);
	}

	@Override
	public HashMap queryPackageBypackageNumber(String packageNumber) {
		return packageDao.queryPackageBypackageNumber(packageNumber );
	}
	@Override
	public Integer sendFetchEmail(ArrayList packageList) {
		return packageDao.sendFetchEmail(packageList);
	}

	@Override
	public Integer sendWeChatNotice(ArrayList expressInfoMap) {
		return packageDao.sendWeChatNotice(expressInfoMap);

	}

	@Override
	public int updatePackagepackageStatusAndFetchCode(ArrayList expressInfoMap) {
		return packageDao.updatePackagepackageStatusAndFetchCode(expressInfoMap);

	}

	@Override
	public HashMap queryLastInsertUnpayPackageInfo(Integer userId) {
		return packageDao.queryLastInsertUnpayPackageInfo(userId);

	}

	@Override
	public JSONObject getPackagePost(String packageNumber, String expressCompanyCode) {
		JSONObject jsonObj = new JSONObject();
		try {
			KdApiSearchDemo api = new KdApiSearchDemo();
			jsonObj = JSON.parseObject(api.orderOnlineByJson(packageNumber,expressCompanyCode));

		} catch (Exception e) {
			e.printStackTrace();

		}
		return jsonObj;


	}

	public static void main(String[] args) {

//		PackageInfoDao packageDao = new PackageInfoDaoDaoImpl();
//		packageDao.queryAllSampleList(1,"张三");
//		System.out.println(packageDao.getPackagePost("1","12"));
//        }
	}
}
