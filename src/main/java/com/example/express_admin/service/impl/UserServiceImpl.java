package com.example.express_admin.service.impl;

import com.example.express_admin.dao.impl.UserDaoImpl;
import com.example.express_admin.pojo.User;
import com.example.express_admin.service.UserService;
import com.example.express_admin.dao.Userdao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private Userdao userdao;
	@Override
	public HashMap queryAllUserList(Integer pageIndex,String keyValue ) {

		return  userdao.queryAllUserList( pageIndex,keyValue);

	}
	@Override
	public User queryUserByUserId(Integer userId ) {

		return  userdao.queryUserByUserId(userId);

	}
	@Override
	public HashMap checkUserByUserNameAndPassWord(String user_name, String user_pwd) {
		return  userdao.checkUserByUserNameAndPassWord( user_name,user_pwd);

	}

	public static void main(String[] args) {
		Userdao userdao = new UserDaoImpl();
		userdao.queryAllUserList(1,"张三");
//        }
	}
}
