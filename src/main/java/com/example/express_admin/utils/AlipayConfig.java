package com.example.express_admin.utils;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id ="2021000117627418";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCYd3v1zT33bmgZN+PHTXJZhKfz+8YadcBDJW+pLnYA67uqW6cM8w6wTxZS5xyGVJUiCF8ZjHeNCw0f8BPLe0WzWwjAnPwYYnGU/hTRlJA8eLKutLy+vRd+RsmWb+x5s87Py+W21eGVnVzLESa85Z4+pSn0gg4AE4rHaeYYvQGKOIqELH1YhbL30C67PXtscwjTMAdQjrhbmFc+mnQ+QCoHNT3l54psAtL8o3fjni1IhPlJvstmBRRsxx/e2BjmH6eOxbzxQNRLCKewKOxqreFi5IGJiQlJqoma7hyiTPOI/4o1W2zJqCM/5e3gt6AQm0bPAtF/s4t9akrmXoTWvXotAgMBAAECggEASNHAdcyaDQRBWdIUy0rVE6ZFJUWZ/2c/9ZH6u74CBDySvDHcHlY0LMcJLX1OZWYeeEuxSB2Lt+SQVzGt8qCk0J3pFz2jNe5Gu1eDD/o5zXgEN2cBQsRDZT9f5qb8NcBIRXzn8SREAtZOzHobm1q78sWlvS8kiqrKvKV6jVQwWRga33rKoxqbz5MyRYxON1zwHp0B1sYJKTHIrQEf9N0534xBy5XVQufv1Tv6/MRfSIqdkTfahDwBlluCj9Un7rhSXBz/U9cko30U9oweqky43BgnQ+TRP7pfMn7o3uRJlUC/H7WY0mSChe5ZwVZM6ktLqOb70n/uToephJcr085HbQKBgQDo7tv3Cx2WCcGOsEWzsCdhnelMrkeTabmYm3ynXA+exir/m5AazecHD5Lird62A1T6AuEZaY5cvl4HNdZbCEFqSaFHVhWAT8/wUbChQ4ydhGsOiGMB0j1IkdBejdMKm5XWtu8ecVHCgww4sVt9mYETY3mISZOcIYUujoKFFfy3iwKBgQCnkLRcSAPTur6EOrXL+YtP1MAz7uvMJ8zy8P7DPbL2gKkip/Dq+F3XYgd6azNde9Jb4HtwaLZMwChMKqHwWKkLc0qohQJqHaO9CjRePQd5/Rp1nPOCKpdoTWpMxDF50OQoMxE3Tk7DpY8Dwoavo+lVcydMv7MrLpoSOWFMqkkMJwKBgGwEMB7smai1F3wKxBfijTCYYNK9UtXKRlW9bN3TNC8lqFTmF7pGV2cP0sapaEixXNufMDFvoKPjZ4kibyfBuUwHnvGMuuXInR3NVAJEZ9W3zUls97SlgOPBMoZWcFJP+fka1eIhHI/941H69PlWkOOj0nzf2cKiTLddQKtkmc+nAoGAIXnqXolQomsrhrVH/qEzMNa7M2TnHNlSu7Jj41OfPloD8FWG7xqBfPcrsUxbbbDAHPTjFFkHGpxFMLpPsfm0hAIbh0FTcynKvYidX7pnlPp438hkabvilcbplr6Xd9PA0TsqlFcDq1iXEtQ5Hjvr8NNZpZ6ix9y+kSd/hdZ7U38CgYByu+lA7zTzz/159GUuojPKyWD8cCMmAEqkKCG65DJ8vFWBNnZCfpzxpeW2vvO5xzJ5313by4dsOMfJ+qRZitbb+0MXXyoco0hHKuACFJepVWmmubVAWKloIygyFveLMQcVTGB9OI0W2vFl9XFVAJfTHDH00DHcP4bsPecwphd7PA==";
    public static  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmHd79c09925oGTfjx01yWYSn8/vGGnXAQyVvqS52AOu7qlunDPMOsE8WUucchlSVIghfGYx3jQsNH/ATy3tFs1sIwJz8GGJxlP4U0ZSQPHiyrrS8vr0XfkbJlm/sebPOz8vlttXhlZ1cyxEmvOWePqUp9IIOABOKx2nmGL0BijiKhCx9WIWy99Auuz17bHMI0zAHUI64W5hXPpp0PkAqBzU95eeKbALS/KN3454tSIT5Sb7LZgUUbMcf3tgY5h+njsW88UDUSwinsCjsaq3hYuSBiYkJSaqJmu4cokzziP+KNVtsyagjP+Xt4LegEJtGzwLRf7OLfWpK5l6E1r16LQIDAQAB";
    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://103.46.128.21:29069/result";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问  return_url.jsp
    public static String return_url ="http://103.46.128.21:29069/result";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
