package com.example.express_admin.utils;



import java.util.ArrayList;
import java.util.HashMap;


public class AssembleSqlUtils {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap assembleWhereSql(ArrayList andConditionList, ArrayList orConditionList, String baseSql, String countSql, String pageSql) {
		HashMap sqlMap = new HashMap();
		String conditionSql = "";
		String itemsSql = "";
		String andconditionSql = "";
		String orconditionSql = "";
		if(andConditionList!=null && andConditionList.size()!=0){
			for(int i =0;i<andConditionList.size();i++) {
				if(i!=andConditionList.size()-1) {
					andconditionSql += andConditionList.get(i) +" and ";
				}else {
					andconditionSql += andConditionList.get(i) +" ";
				}
			}

		}
		if(orConditionList!=null && orConditionList.size()!=0){
			for(int i =0;i<orConditionList.size();i++) {
				if(i!=orConditionList.size()-1) {
					orconditionSql += orConditionList.get(i) +" or ";
				}else {
					orconditionSql += orConditionList.get(i) +" ";
				}
			}
		}
		if(andconditionSql !="" && orconditionSql !=""){
			conditionSql += "("+andconditionSql+") and ("+orconditionSql+")";
		}
		else{
			if(andconditionSql !=""){
				conditionSql += andconditionSql;
			}
			if( orconditionSql !=""){
				conditionSql +=  orconditionSql;
			}
		}
		if(conditionSql!=""){
			conditionSql = " where"+ conditionSql;
			itemsSql = baseSql + conditionSql +pageSql;
			countSql = countSql+conditionSql;
		}else{
			itemsSql = baseSql +pageSql;
			countSql = countSql;
		}
		System.out.println("数据库查询语句:"+itemsSql);
		sqlMap.put("itemsSql",itemsSql);
		sqlMap.put("countSql",countSql);
		return sqlMap;
	}




}
