package com.example.express_admin.utils;

import java.util.Random;

/**
 * @PACKAGE_NAME: com.example.express_admin.utils
 * @NAME: codeUtils
 * @USER: jacky
 * @DATE: 2023/4/17
 * @TIME: 15:30
 * @YEAR: 2023
 * @MONTH: 04
 * @MONTH_NAME_SHORT: 4月
 * @MONTH_NAME_FULL: 四月
 * @DAY: 17
 * @DAY_NAME_SHORT: 周一
 * @DAY_NAME_FULL: 星期一
 * @HOUR: 15
 * @MINUTE: 30
 * @PROJECT_NAME: store-admin-system_for_mongodb
 */
public class CodeUtils
{
    public static String getStringRandom() {
        Random random = new Random();
        //把随机生成的数字转成字符串
        String str = String.valueOf(random.nextInt(9));
        for (int i = 0; i < 4; i++) {
            str += random.nextInt(9);
        }
        return str;
    }
}
