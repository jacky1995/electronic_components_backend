package com.example.express_admin.utils;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/*map���Ӻ�*/
public class FileUtils {

//	写入文件到本地
public void writeFileToLocal(MultipartFile file,String filePath) throws IOException {
	InputStream fileInputStream =file.getInputStream();
	FileOutputStream fileOutputStream = null;
	try {

		fileOutputStream = new FileOutputStream(filePath);//这里可以改路径
		IOUtils.copy(fileInputStream, fileOutputStream);
		fileOutputStream.flush();
		System.out.println("文件写入成功:"+filePath);
	} catch (IOException e) {
		e.printStackTrace();
	}finally {
		try {
			if (fileInputStream != null) {
				fileInputStream.close();
			}
			if (fileOutputStream != null) {
				fileOutputStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}


	public void deleteFile(String filePath) {
		boolean flag = false;
		//根据路径创建文件对象
		File file = new File(filePath);
		//路径是个文件且不为空时删除文件
		if (file.isFile() && file.exists()) {

			flag = file.delete();
			System.out.println("删除文件成功");
		} else{
			System.out.println("文件不存在："+filePath);
		}

	}

	public static void main(String[] args) {

		String str = "http://127.0.0.1:8888/static/upload/3_苹果手机_8901681909915_.pic.jpg";
		int lastIndex = str.lastIndexOf("/")+1;
//		String url = str.substring(index, str.length());
//
		String local_image_path= str.substring(lastIndex,str.length());
		System.out.println(local_image_path);


	}

}
