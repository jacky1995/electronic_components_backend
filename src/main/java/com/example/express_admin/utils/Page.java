package com.example.express_admin.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class Page {
	public HashMap<Object, Object> getPage(Integer pageIndex,String sql) {

        //数据库资源对象放外面，以便finally关闭
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        Integer pageSize = 1;
    	PageModel pageModel =new PageModel();
        try{
            //获取连接
            conn = JdbcUtils.getConnection();
            //创建语句对象
            st = conn.createStatement();
            //查询
            rs = st.executeQuery(sql);
        	HashMap pageModelMap =  new HashMap<>();
//        	totalRecordSum
//        	totalPageSum
//        	pageIndex

            while (rs.next()){
            	pageModel.setTotalRecordSum(rs.getInt(1));
                pageModel.setPageIndex(pageIndex);
//            	pageModelMap.put("totalRecordSum", pageModel.getTotalRecordSum());
//            	pageModelMap.put("totalPageSum", pageModel.getTotalPageSum());
//            	pageModelMap.put("pageIndex", pageIndex);

                pageModelMap.put("count", true);
                pageModelMap.put("pageSizeZero", false);
                pageModelMap.put("reasonable", true);
                pageModelMap.put("pageNum", pageIndex);
                pageModelMap.put("pageSize", pageModel.getPagesize());
                pageModelMap.put("startRow", pageModel.getStartRow());
                pageModelMap.put("endRow", pageModel.getEndRow());
                pageModelMap.put("totalRecordSum", pageModel.getTotalRecordSum());
                pageModelMap.put("totalPageSum", pageModel.getTotalPageSum());

            }
            return pageModelMap;

        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            JdbcUtils.close(conn);
        }
        return null;
	}
    public static void main(String[] args) throws SQLException {
    	Page  pageDao = new Page();
    	System.out.println(pageDao.getPage(2,"select count(1) from user_inf"));
//    	pageModel
//    	totalRecordSum
//    	totalPageSum
//    	pageIndex
    }
}
