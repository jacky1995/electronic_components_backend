package com.example.express_admin.utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
@Slf4j
@Mapper
public class RSAUtil {
    private static final String KEY_ALGORITHM = "RSA";
    private static final String PUBLIC_KEY = "RSAPublicKey";
    private static final String PRIVATE_KEY = "RSAPrivateKey";
    private static final int MAX_ENCRYPT_BLOCK = 117;
    private static final int MAX_DECRYPT_BLOCK = 128;
    private static PublicKey publicKey;
    private static PrivateKey privateKey;
    public static String publicKeyPem;
    public static String privatePem;

    public static byte[] decryptBASE64(String key) {
        return Base64.getDecoder().decode(key);
    }

    public static String encryptBASE64(byte[] key) {
        return new String(Base64.getEncoder().encode(key));
    }

    static {
        Map<String, Object> keyMap = null;
        try {
            keyMap = initKey(1024);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        publicKey = (PublicKey) keyMap.get(PUBLIC_KEY);
        privateKey = (PrivateKey) keyMap.get(PRIVATE_KEY);
        // 获得 map 中的公钥对象，转为 key 对象
        Key key = (Key) keyMap.get(PUBLIC_KEY);
        publicKeyPem = encryptBASE64(key.getEncoded());
        // 获得 map 中的私钥对象，转为 key 对象
        Key key2 = (Key) keyMap.get(PRIVATE_KEY);
        // 编码返回字符串
        privatePem =  encryptBASE64(key2.getEncoded());

    }


    public static Map<String, Object> initKey(int keysize) throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        keyPairGen.initialize(keysize);
        KeyPair keyPair = keyPairGen.generateKeyPair();
        RSAPublicKey rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();
        Map<String, Object> keyMap = new HashMap<>(2);

        keyMap.put(PUBLIC_KEY, rsaPublicKey);
        keyMap.put(PRIVATE_KEY, rsaPrivateKey);
        return keyMap;
    }
    public static String encrypt(String plainText) throws Exception {

        log.info("明文:[{}],长度:[{}]", plainText, plainText.length());
        byte[] plainTextArray = plainText.getBytes("UTF-8");
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);

        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        int inputLen = plainTextArray.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        int i = 0;
        byte[] cache;
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(plainTextArray, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(plainTextArray, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptText = out.toByteArray();
        out.close();
        return Base64.getEncoder().encodeToString(encryptText);
    }
    public static String decrypt(String encryptTextHex) throws Exception {
        byte[] encryptText = Base64.getDecoder().decode(encryptTextHex);
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        int inputLen = encryptText.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(encryptText, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(encryptText, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] plainText = out.toByteArray();
        out.close();
        return new String(plainText, "UTF-8");
    }

    public static void main(String[] args) throws Exception {
        String content = "1";

//        RSAUtil rsatil = new RSAUtil();


        String encryptText = encrypt(content);
        log.info("密文:[{}]", encryptText);
        String decryptText = decrypt(encryptText);
        log.info("明文:[{}]", decryptText);
    }

}

