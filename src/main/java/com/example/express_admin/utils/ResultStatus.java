package com.example.express_admin.utils;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@ToString
@Getter
public enum ResultStatus {

    SUCCESS(HttpStatus.OK, 0, "OK"),
    Failure(HttpStatus.BAD_REQUEST, 1, "Bad Request"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, 1, "Internal Server Error"),;

    /** 返回的HTTP状态码,  符合http请求 */
    private HttpStatus httpStatus;
    /** 业务异常码 */
    private Integer error;
    /** 业务异常信息描述 */
    private String msg;

    ResultStatus(HttpStatus httpStatus, Integer error, String msg) {
        this.httpStatus = httpStatus;
        this.error = error;
        this.msg = msg;
    }
}  