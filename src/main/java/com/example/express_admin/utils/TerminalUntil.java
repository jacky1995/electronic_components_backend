package com.example.express_admin.utils;

import com.alibaba.druid.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @PACKAGE_NAME: com.example.express_admin.utils
 * @NAME: TerminalUntil
 * @USER: jacky
 * @DATE: 2023/4/28
 * @TIME: 18:55
 * @YEAR: 2023
 * @MONTH: 04
 * @MONTH_NAME_SHORT: 4月
 * @MONTH_NAME_FULL: 四月
 * @DAY: 28
 * @DAY_NAME_SHORT: 周五
 * @DAY_NAME_FULL: 星期五
 * @HOUR: 18
 * @MINUTE: 55
 * @PROJECT_NAME: express-admin-system
 */
public class TerminalUntil {
    public static String execCmdOrder(String cmdCommand) {

        if (StringUtils.isEmpty(cmdCommand)){
            throw new RuntimeException("需要执行的命令不可以为空");
        }

        /*if (***其他判断条件***){
            throw new RuntimeException("不满足判断条件的原因");
        }*/

        //组装cmd 命令
        String cmd= cmdCommand ;
        BufferedReader br=null;
        Process process=null;
        //执行命令结果
        StringBuilder result = null;
        try {
            //执行cmd命令
            process=Runtime.getRuntime().exec(cmd);
            //获取cmd命令的输出结果
            br=new BufferedReader(new InputStreamReader(process.getInputStream()));
            result= new StringBuilder();
            String tmp;
            //组装命令执行结束后返回值
            while ((tmp= br.readLine())!=null){
                result.append(tmp).append("\n");
            }
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }finally {
            if (br!=null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (process != null) {
                process.destroy();
            }
            System.out.println("执行命令结束以后控制台返回结果为 ：" + result);
        }
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(execCmdOrder("ngrok http 8888"));
    }
}
