package com.example.express_admin.utils;


import com.alibaba.fastjson.JSONObject;
import com.example.express_admin.pojo.WeChatTemplateMsg;
import com.example.express_admin.pojo.WechatAccount;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XCXMain {


    RestTemplate restTemplate = new RestTemplate();
    WechatAccount wechatAccount = new WechatAccount();
    private static final Logger log = LoggerFactory.getLogger(XCXMain .class);

    public String getAccessToken(String appid,String appsecret) {

        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + appsecret;
        String response = restTemplate.getForObject(url, String.class);
        JSONObject object = JSONObject.parseObject(response);
        String accessToken = object.getString("access_token");
        return accessToken;
    }
    public List<String> getOpenIdsFromWX(String accessToken) {
        String url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + accessToken;
        ResponseEntity<String> response = restTemplate.postForEntity(url, null, String.class);
        JSONObject object = JSONObject.parseObject(response.getBody());

        List<String> openIds = JSONObject.parseArray(object.getJSONObject("data").getString("openid"), String.class);
        return openIds;
    }

    public List<String> getAllOpenIds(String accessToken){

        // openId代表一个唯一微信用户，即微信消息的接收人

        List<String> openIds = getOpenIdsFromWX(accessToken);
        return openIds;
    }
    public void sendMessage(HashMap expressInfoMap){
        // 模板参数
        Map<String, WeChatTemplateMsg> sendMag = new HashMap<String, WeChatTemplateMsg>();
        String accessToken = getAccessToken(wechatAccount.getMpAppId(),wechatAccount.getMpAppSecret());
//        List<String> openIds = getAllOpenIds(accessToken);
        String openId = (String) expressInfoMap.get("openId");
        // 公众号的模板id(也有相应的接口可以查询到)
        String templateId =  wechatAccount.getTemplateId();
        // 微信的基础accessToken
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken;
        sendMag.put("expressCompanyName", new WeChatTemplateMsg((String) expressInfoMap.get("expressCompanyName"),"#000000"));
        sendMag.put("packageNumber", new WeChatTemplateMsg((String) expressInfoMap.get("packageNumber"),"#000000"));
        sendMag.put("fetchCode", new WeChatTemplateMsg((String) expressInfoMap.get("fetchCode"),"#000000"));
//        sendMag.put("address", new WeChatTemplateMsg((String) expressInfoMap.get("address"),"#000000"));
        sendMag.put("address", new WeChatTemplateMsg("广州应用科技学院天天购驿站","#000000"));
        RestTemplate restTemplate = new RestTemplate();
        //拼接base参数
        Map<String, Object> sendBody = new HashMap<>();

        sendBody.put("touser", openId);               // openId
//        sendBody.put("url", "https://www.baidu.com");  //跳转网页url
        sendBody.put("data", sendMag);                   // 模板参数
        sendBody.put("template_id", templateId);      // 模板Id
        ResponseEntity<String> response = restTemplate.postForEntity(requestUrl, sendBody, String.class);
        com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(response.getBody());
        String messageCode = jsonObject.getString("errcode");
        String msgId = jsonObject.getString("msgid");
        System.out.println("messageCode : " + messageCode + ", msgId: " +msgId);
    }


    public JSONObject getWechatUserInfo(String accessToken, String openId) {
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + accessToken + "&openid=" + openId;
        String response = restTemplate.getForObject(requestUrl, String.class);
        JSONObject userInfo = JSONObject.parseObject(response);
        return userInfo;
    }

    public static void main(String[] args) {
        XCXMain xCXMain = new XCXMain();
        WechatAccount wechatAccount = new WechatAccount();
        String accessToken = xCXMain.getAccessToken(wechatAccount.getMpAppId(),wechatAccount.getMpAppSecret());
        List<String> openIds = xCXMain.getAllOpenIds(accessToken);
        System.out.println(xCXMain.getWechatUserInfo(accessToken,openIds.get(0)));

//        HashMap expressInfoMap = new HashMap();
//        expressInfoMap.put("expressCompanyName","圆通");
//        expressInfoMap.put("packageNumber","123");
//        expressInfoMap.put("fetchCode","9527");
//        expressInfoMap.put("address","松田");
//        xCXMain.sendMessage(expressInfoMap);

    }
}