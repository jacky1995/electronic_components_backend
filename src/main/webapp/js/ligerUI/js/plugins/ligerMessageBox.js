﻿/**
* jQuery ligerUI 1.3.2
* 
* http://ligerui.com
*  
* Author daomi 2015 [ gd_star@163.com ] 
* 
*/
(function ($)
{

    $.ligermsgBox = function (options)
    {
        return $.ligerui.run.call(null, "ligermsgBox", arguments, { isStatic: true });
    };


    $.ligerDefaults.msgBox = {
        isDrag: true
    };

    $.ligerMethos.msgBox = {};

    $.ligerui.controls.msgBox = function (options)
    {
        $.ligerui.controls.msgBox.base.constructor.call(this, null, options);
    };
    $.ligerui.controls.msgBox.ligerExtend($.ligerui.core.UIComponent, {
        __getType: function ()
        {
            return 'msgBox';
        },
        __idPrev: function ()
        {
            return 'msgBox';
        },
        _extendMethods: function ()
        {
            return $.ligerMethos.msgBox;
        },
        _render: function ()
        {
            var g = this, p = this.options;
            var msgBoxHTML = "";
            msgBoxHTML += '<div class="l-msgbox">';
            msgBoxHTML += '        <div class="l-msgbox-lt"></div><div class="l-msgbox-rt"></div>';
            msgBoxHTML += '        <div class="l-msgbox-l"></div><div class="l-msgbox-r"></div> ';
            msgBoxHTML += '        <div class="l-msgbox-image"></div>';
            msgBoxHTML += '        <div class="l-msgbox-title">';
            msgBoxHTML += '            <div class="l-msgbox-title-inner"></div>';
            msgBoxHTML += '            <div class="l-msgbox-close"></div>';
            msgBoxHTML += '        </div>';
            msgBoxHTML += '        <div class="l-msgbox-content">';
            msgBoxHTML += '        </div>';
            msgBoxHTML += '        <div class="l-msgbox-buttons"><div class="l-msgbox-buttons-inner">';
            msgBoxHTML += '        </div></div>';
            msgBoxHTML += '    </div>';
            g.msgBox = $(msgBoxHTML);
            $('body').append(g.msgBox);
            g.msgBox.close = function ()
            {
                g._removeWindowMask();
                g.msgBox.remove();
            };
            //设置参数属性
            p.width && g.msgBox.width(p.width);
            p.title && $(".l-msgbox-title-inner", g.msgBox).html(p.title);
            p.content && $(".l-msgbox-content", g.msgBox).html(p.content);
            if (p.buttons)
            {
                $(p.buttons).each(function (i, item)
                {
                    var btn = $('<div class="l-msgbox-btn"><div class="l-msgbox-btn-l"></div><div class="l-msgbox-btn-r"></div><div class="l-msgbox-btn-inner"></div></div>');
                    $(".l-msgbox-btn-inner", btn).html(item.text);
                    $(".l-msgbox-buttons-inner", g.msgBox).append(btn);
                    item.width && btn.width(item.width);
                    item.onclick && btn.click(function () { item.onclick(item, i, g.msgBox) });
                });
                $(".l-msgbox-buttons-inner", g.msgBox).append("<div class='l-clear'></div>");
            }
            var boxWidth = g.msgBox.width();
            var sumBtnWidth = 0;
            $(".l-msgbox-buttons-inner .l-msgbox-btn", g.msgBox).each(function ()
            {
                sumBtnWidth += $(this).width();
            });
            $(".l-msgbox-buttons-inner", g.msgBox).css({ marginLeft: parseInt((boxWidth - sumBtnWidth) * 0.5) });
            //设置背景、拖动支持 和设置图片
            g._applyWindowMask();
            g._applyDrag();
            g._setImage();

            //位置初始化
            var left = 0;
            var top = 0;
            var width = p.width || g.msgBox.width();
            if (p.left != null) left = p.left;
            else p.left = left = 0.5 * ($(window).width() - width);
            if (p.top != null) top = p.top;
            else p.top = top = 0.5 * ($(window).height() - g.msgBox.height()) + $(window).scrollTop() - 10;
            if (left < 0) p.left = left = 0;
            if (top < 0) p.top = top = 0;
            g.msgBox.css({ left: left, top: top });

            //设置事件
            $(".l-msgbox-btn", g.msgBox).hover(function ()
            {
                $(this).addClass("l-msgbox-btn-over");
            }, function ()
            {
                $(this).removeClass("l-msgbox-btn-over");
            });
            $(".l-msgbox-close", g.msgBox).hover(function ()
            {
                $(this).addClass("l-msgbox-close-over");
            }, function ()
            {
                $(this).removeClass("l-msgbox-close-over");
            }).click(function ()
            {
                g.msgBox.close();
            });
            g.set(p);
        },
        close: function ()
        {
            var g = this, p = this.options;
            this.g._removeWindowMask();
            this.msgBox.remove();
        },
        _applyWindowMask: function ()
        {
            var g = this, p = this.options;
            $(".l-window-mask").remove();
            $("<div class='l-window-mask' style='display: block;'></div>").appendTo($("body"));
        },
        _removeWindowMask: function ()
        {
            var g = this, p = this.options;
            $(".l-window-mask").remove();
        },
        _applyDrag: function ()
        {
            var g = this, p = this.options;
            if (p.isDrag && $.fn.ligerDrag)
                g.msgBox.ligerDrag({ handler: '.l-msgbox-title-inner', animate: false });
        },
        _setImage: function ()
        {
            var g = this, p = this.options;
            if (p.type)
            {
                if (p.type == 'success' || p.type == 'donne')
                {
                    $(".l-msgbox-image", g.msgBox).addClass("l-msgbox-image-donne").show();
                    $(".l-msgbox-content", g.msgBox).css({ paddingLeft: 64, paddingBottom: 30 });
                }
                else if (p.type == 'error')
                {
                    $(".l-msgbox-image", g.msgBox).addClass("l-msgbox-image-error").show();
                    $(".l-msgbox-content", g.msgBox).css({ paddingLeft: 64, paddingBottom: 30 });
                }
                else if (p.type == 'warn')
                {
                    $(".l-msgbox-image", g.msgBox).addClass("l-msgbox-image-warn").show();
                    $(".l-msgbox-content", g.msgBox).css({ paddingLeft: 64, paddingBottom: 30 });
                }
                else if (p.type == 'question')
                {
                    $(".l-msgbox-image", g.msgBox).addClass("l-msgbox-image-question").show();
                    $(".l-msgbox-content", g.msgBox).css({ paddingLeft: 64, paddingBottom: 40 });
                }
            }
        }
    });


    $.ligermsgBox.show = function (p)
    {
        return $.ligermsgBox(p);
    };
    $.ligermsgBox.alert = function (title, content, type, onBtnClick)
    {
        title = title || "";
        content = content || title;
        var onclick = function (item, index, msgBox)
        {
            msgBox.close();
            if (onBtnClick)
                onBtnClick(item, index, msgBox);
        };
        p = {
            title: title,
            content: content,
            buttons: [{ text: '确定', onclick: onclick}]
        };
        if (type) p.type = type;
        return $.ligermsgBox(p);
    };
    $.ligermsgBox.confirm = function (title, content, callback)
    {
        var onclick = function (item, index, msgBox)
        {
            msgBox.close();
            if (callback)
            {
                callback(index == 0);
            }
        };
        p = {
            type: 'question',
            title: title,
            content: content,
            buttons: [{ text: '是', onclick: onclick }, { text: '否', onclick: onclick}]
        };
        return $.ligermsgBox(p);
    };
    $.ligermsgBox.success = function (title, content, onBtnClick)
    {
        return $.ligermsgBox.alert(title, content, 'success', onBtnClick);
    };
    $.ligermsgBox.error = function (title, content, onBtnClick)
    {
        return $.ligermsgBox.alert(title, content, 'error', onBtnClick);
    };
    $.ligermsgBox.warn = function (title, content, onBtnClick)
    {
        return $.ligermsgBox.alert(title, content, 'warn', onBtnClick);
    };
    $.ligermsgBox.question = function (title, content)
    {
        return $.ligermsgBox.alert(title, content, 'question');
    };


})(jQuery);