package com.example.express_admin;


import com.example.express_admin.utils.CodeUtils;
import com.example.express_admin.utils.SendEmailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class mailTests {
    @Autowired
    private SendEmailService sendEmailService;
    @Test
    public void sendMailTest() {
        CodeUtils codeUtils = new CodeUtils();
        String to = "787123867@qq.com";
        String subject = "您有一封来自汕头市云上禅端技术开发有限公司的的邮件";
        String text =  String.format(

                "<h1>【汕头市云上禅端技术开发有限公司】验证码:<span style=font-weight: bold;color: red;>%s</span>，此验证码用于深圳绘王身份验证，请勿泄露。<h1> ",codeUtils.getStringRandom());

        sendEmailService.sendSimpleEmail(to,subject,text);

    }

}

