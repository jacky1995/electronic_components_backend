package com.example.express_admin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;


@RunWith(SpringRunner.class)

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class mongoTests {
    Query query = new Query();
    @Autowired
    private MongoTemplate mongoTemplate;


    @Test
    public void selectUser() {
//
//
//            分页
        int currentPage = 1;// 0,1相同
        int pageSize = 2;
        Criteria criteria = new Criteria();

        LookupOperation lookupOperation = Aggregation.lookup(
                "t_depart",
                "departId",
                "departId",
                "t_depart");
        criteria.orOperator(
                Criteria.where("userName").regex("张学友"),
                Criteria.where("t_depart.departName").regex("研发")
        );
        AggregationOperation matchFu = Aggregation.match(criteria);
        Aggregation aggregation = Aggregation.newAggregation(matchFu,lookupOperation,
                Aggregation.skip(pageSize > 1 ? (currentPage - 1) * pageSize : 0), Aggregation.limit(pageSize),
                Aggregation.unwind("t_depart",true)

        );

        List<Map> gradeList = mongoTemplate.aggregate(aggregation, "t_user", Map.class).getMappedResults();
        gradeList.forEach(System.out::println);



//        user = userRepositoryMapper.queryUserByUserNameAndPassWord("admin","Aa123456");
//        System.out.println(user);


//        System.out.println(userRepositoryMapper.queryAllUserList("admin", "Aa123456"));
//        System.out.println(userRepositoryMapper.queryUserListByPage(1, 1));
//        System.out.println(userRepositoryMapper.queryUserListByPageAndKey(0, 5,"企划"));
//        System.out.println(userRepositoryMapper.queryUserByuserId(1));
//        System.out.println(userRepositoryMapper.queryDepart());
//        System.out.println(userRepositoryMapper.updateUserByUserId(1,"Aa123456",1,"11111111111","123@qq.com"));
//        System.out.println(userRepositoryMapper.addUser("Aa123456",1,"11111111111","123@qq.com","test"));
//        System.out.println(userRepositoryMapper.deleteUserByuserId(6));
//        System.out.println(userRepositoryMapper.queryeUserByUserNameAndMobile("11111111111","admin"));
//        System.out.println(userRepositoryMapper.updateUserByUserNameAndMobile("11111111111","Aa123456","admin"));


//        System.out.println(sampleRepositoryMapper.querySampleModelBySampleTypeId(2));
//        System.out.println(sampleRepositoryMapper.querySampleModelBySampleTypeId(2));


//        mongo查询

////  查询所有
//        Criteria criteria =new Criteria();
//
//        criteria.orOperator(
////                Criteria.where("userName").regex("^.*"+"张学友"+".*$")
//                Criteria.where("userName").is("admin").and("userPwd").is("Aa888888")
//        );
//        query.addCriteria(criteria);
//        System.out.println(mongoTemplate.find(query, User.class));


    }

}

